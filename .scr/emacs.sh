git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d
git clone https://github.com/redfish64/idris2-mode ~/.emacs.d/idris2-mode
wget -O ~/.emacs.d/idris2-mode/prop-menu.el https://raw.githubusercontent.com/david-christiansen/prop-menu-el/master/prop-menu.el

echo '(add-to-list '"'"'load-path "~/.emacs.d/idris2-mode/")' >> ~/.emacs.d/init.el
echo '(require '"'"'idris2-mode)' >> ~/.emacs.d/init.el
