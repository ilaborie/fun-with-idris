#!/usr/bin/env bash
#arg: ctx | dev:gitpod | DOCKER_CTX | Docker context, dev by default
#opt: --push | flag | PUSH | Push the image

DOCKER_CTX=${DOCKER_CTX:-dev}
if [ "$DOCKER_CTX" = 'gitpod' ]; then
  IMAGE_TAG="${IMAGE_TAG}-gitpod"
fi

IMAGE="$IMAGE_NAME:$IMAGE_TAG"

pwd::push "${PROJECT_HOME}/etc"
docker build . \
    --progress=plain \
    -f "$DOCKER_CTX.Dockerfile"  \
    -t "$IMAGE"
if [ -n "$PUSH" ]; then
  docker push "$IMAGE"
fi
pwd::pop

