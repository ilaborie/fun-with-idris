FROM ubuntu:23.10

ARG USERNAME=ubuntu
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Install dependencies
RUN echo "LC_ALL=en_US.UTF-8" >> /etc/environment
RUN echo "LANG=en_US.UTF-8" >> /etc/environment

RUN apt-get update \
 && apt-get install -y sudo curl gpg git build-essential chezscheme libgmp3-dev rlwrap

RUN echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

USER ubuntu

# Installing Idris2 through idris2-pack
ENV SCHEME=chezscheme

RUN curl -fsSL https://raw.githubusercontent.com/stefan-hoeck/idris2-pack/main/install.bash > /tmp/pack-install.sh \
 && chmod +x /tmp/pack-install.sh \
 && echo "$SCHEME" | /tmp/pack-install.sh

ENV PATH="/home/ubuntu/.pack/bin:$PATH"

# Installing idris2-lsp for integration with vscode
RUN echo "yes" | pack install-app idris2-lsp
RUN pack install elab-util

# Installing scr
RUN mkdir -p /home/ubuntu/.local/bin
ENV PATH="/home/ubuntu/.local/bin:$PATH"
RUN curl https://gitlab.com/glmxndr/scr/-/raw/main/scr > /home/ubuntu/.local/bin/scr
RUN chmod +x /home/ubuntu/.local/bin/scr
RUN echo 'source <(scr --completions)' >> /home/ubuntu/.bashrc
