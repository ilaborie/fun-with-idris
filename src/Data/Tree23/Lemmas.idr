module Data.Tree23.Lemmas

import public Data.Tree23.Types
import Control.Relation
import Decidable.Order.Strict

%default total


%hint
export
lemma_elem_leaf_eq : Elem x (Leaf y _) -> x = y
lemma_elem_leaf_eq InLeaf = Refl


%hint
export
lemma_elem_branch2 :  {ctx : TCtx}
                   -> {a, b, c, d, x : ctx.key}
                   -> {l : Tree23 ctx a b depth} 
                   -> {r : Tree23 ctx c d depth} 
                   -> {auto 0 r0 : ctx.rel b c}
                   -> Elem x (Branch2 l r)
                   -> Either (Elem x l) (Elem x r)
lemma_elem_branch2 (InB2L y) = Left y
lemma_elem_branch2 (InB2R y) = Right y

%hint
export
lemma_elem_branch3 :  {ctx : TCtx}
                   -> {a, b, c, d, e, f, x: ctx.key}
                   -> {l : Tree23 ctx a b depth} 
                   -> {m : Tree23 ctx c d depth} 
                   -> {r : Tree23 ctx e f depth} 
                   -> {auto 0 r1 : ctx.rel b c}
                   -> {auto 0 r2 : ctx.rel d e}
                   -> Elem x (Branch3 l m r) 
                   -> Either (Elem x m) (Either (Elem x l) (Elem x r))
lemma_elem_branch3 (InB3M y) = Left y
lemma_elem_branch3 (InB3L y) = Right (Left y)
lemma_elem_branch3 (InB3R y) = Right (Right y)

%hint
export
irrefl : {ctx : TCtx} -> {x,y : ctx.key} -> x = y -> Not (ctx.rel x y)
irrefl Refl = irreflexive @{ctx.irr}

export
trans : {ctx : TCtx} -> {x,y,z : ctx.key} -> (xy: ctx.rel x y) -> (yz: ctx.rel y z) -> ctx.rel x z
trans = transitive @{ctx.tra}

%hint
export
asym : {ctx : TCtx} -> {a,b : ctx.key} -> ctx.rel a b -> Not (ctx.rel b a)
asym ab ba = irrefl Refl (trans {x=a} {y=b} {z=a} ab ba)


||| The lowest element of a tree belongs to it
%hint
export
elemLow : (t:Tree23 ctx lk hk d) -> Elem lk t
elemLow (Leaf x v) = InLeaf
elemLow (Branch2 l r) = InB2L (elemLow l)
elemLow (Branch3 l m r) = InB3L (elemLow l)

||| The highest element of a tree belongs to it
%hint
export
elemHigh : (t:Tree23 ctx lk hk d) -> Elem hk t
elemHigh (Leaf x v) = InLeaf
elemHigh (Branch2 l r) = InB2R (elemHigh r)
elemHigh (Branch3 l m r) = InB3R (elemHigh r)



[TransLTE] StrictOrdered k r => Transitive k (LTE' k r) where
  transitive (LT' w) (LT' v) = LT' $ transitive w v
  transitive (LT' w) (EQ' prf) = LT' (rewrite (sym prf) in w)
  transitive (EQ' prf) (LT' w) = LT' (rewrite prf in w)
  transitive (EQ' prf) (EQ' prf1) = EQ' (transitive prf prf1)

%hint
export
transLTE : {ctx : TCtx}
      -> {a, b, c : ctx.key}
      -> LTE' ctx.key ctx.rel a b
      -> LTE' ctx.key ctx.rel b c
      -> LTE' ctx.key ctx.rel a c
transLTE = transitive @{TransLTE @{ctx.ord}}

||| Low bound of a tree is equal to or lower than its high bound 
%hint
export
0 tree_lte_bounds :  {ctx : TCtx}
                -> {a, f : ctx.key}
                -> (t : Tree23 ctx a f depth)
                -> LTE' ctx.key ctx.rel a f
tree_lte_bounds (Leaf lk y) = EQ' Refl
tree_lte_bounds (Branch2 l {r0} r) = 
  transLTE 
    (transLTE (tree_lte_bounds l) (LT' r0)) 
    (tree_lte_bounds r)
tree_lte_bounds (Branch3 l {r1} m {r2} r) = 
   transLTE
    (transLTE
      (transLTE
        (transLTE (tree_lte_bounds l) (LT' r1)) 
        (tree_lte_bounds m))
      (LT' r2)
    )
    (tree_lte_bounds r)



infixr 6 !<!
infixr 6 ~<!
infixr 6 !<~

export
(!<!) : {ctx : TCtx} -> {x,y,z : ctx.key} -> (xy: ctx.rel x y) -> (yz: ctx.rel y z) -> ctx.rel x z
(!<!) = transitive @{ctx.tra}


export
0 trans_tree_lt :   {ctx : TCtx}
                -> {a, f, k : ctx.key}
                -> (t : Tree23 ctx a f depth)
                -> (l : ctx.rel f k)
                -> ctx.rel a k
trans_tree_lt t l with (tree_lte_bounds t)
  trans_tree_lt t l | (LT' x) = x !<! l
  trans_tree_lt t l | (EQ' prf) = rewrite prf in l


export
0 (~<!) :  {ctx : TCtx}
        -> {a, f, k : ctx.key}
        -> (t : Tree23 ctx a f depth)
        -> (l : ctx.rel f k)
        -> ctx.rel a k
(~<!) = trans_tree_lt


export
0 trans_lt_tree :   {ctx : TCtx}
                -> {a, f, k : ctx.key}
                -> (l : ctx.rel k a)
                -> (t : Tree23 ctx a f depth)
                -> ctx.rel k f
trans_lt_tree l t with (tree_lte_bounds t)
  trans_lt_tree l t | (LT' x) = l !<! x
  trans_lt_tree l t | (EQ' prf) = rewrite (sym prf) in l

export
0 (!<~) :  {ctx : TCtx}
        -> {a, f, k : ctx.key}
        -> (l : ctx.rel k a)
        -> (t : Tree23 ctx a f depth)
        -> ctx.rel k f
(!<~) = trans_lt_tree


export
0 lemma_treeS_lt : {ctx:TCtx}
               -> {a,f: ctx.key}
               -> Tree23 ctx a f (S depth)
               -> ctx.rel a f               
lemma_treeS_lt (Branch2 {r0} l r) = trans_lt_tree (trans_tree_lt l r0) r
lemma_treeS_lt (Branch3 l {r1} m {r2} r) = trans {ctx} (trans_lt_tree (trans_tree_lt l r1) m) (trans_lt_tree r2 r)


||| Elements of trees are below or equal to its high bound
%hint
export
0 lemma_elem_lte_high :  {ctx : TCtx}
                    -> {a, f, x : ctx.key}
                    -> (t : Tree23 ctx a f depth)
                    -> Elem x t
                    -> LTE' ctx.key ctx.rel x f
lemma_elem_lte_high (Leaf a v) InLeaf = EQ' Refl
lemma_elem_lte_high (Branch2 l r) (InB2L {r0} eleml) = 
  transLTE (lemma_elem_lte_high l eleml) 
           (transLTE (LT' r0) (tree_lte_bounds r))
lemma_elem_lte_high (Branch2 l r) (InB2R {r0} elemr) = 
  lemma_elem_lte_high r elemr
lemma_elem_lte_high (Branch3 l m r) (InB3L {r1} {r2} eleml) = 
  transLTE (lemma_elem_lte_high l eleml) 
           (transLTE (LT' r1) 
                     (transLTE (tree_lte_bounds m)
                     (transLTE (LT' r2)
                               (tree_lte_bounds r))))
                               
lemma_elem_lte_high (Branch3 l m r) (InB3M {r2} elemm) = 
  transLTE (lemma_elem_lte_high m elemm)
           (transLTE (LT' r2)
                     (tree_lte_bounds r))
lemma_elem_lte_high (Branch3 l m r) (InB3R elemr) =
  lemma_elem_lte_high r elemr

||| Elements of trees are equal to or above its low bound
%hint
export
0 lemma_elem_low_lte :  {ctx: TCtx}
                   -> {a, f, x: ctx.key}
                   -> (t: Tree23 ctx a f depth)
                   -> Elem x t
                   -> LTE' ctx.key ctx.rel a x
lemma_elem_low_lte (Leaf a v) InLeaf = EQ' Refl
lemma_elem_low_lte (Branch2 l r) (InB2L eleml) = lemma_elem_low_lte l eleml
lemma_elem_low_lte (Branch2 l r) (InB2R {r0} elemr) = transLTE (transLTE (tree_lte_bounds l) (LT' r0)) (lemma_elem_low_lte r elemr)
lemma_elem_low_lte (Branch3 l m r) (InB3L eleml) = lemma_elem_low_lte l eleml
lemma_elem_low_lte (Branch3 l m r) (InB3M {r1} elemm) = transLTE (transLTE (tree_lte_bounds l) (LT' r1)) (lemma_elem_low_lte m elemm)
lemma_elem_low_lte (Branch3 l m r) (InB3R {r1} {r2} elemr) = 
  transLTE 
    (transLTE 
      (tree_lte_bounds l) 
      (LT' r1)
    ) 
    (transLTE
      (transLTE 
        (tree_lte_bounds m) 
        (LT' r2)
      )
      (lemma_elem_low_lte r elemr)
    )

||| Elements of trees are not below its low bound
%hint
export
0 lemma_elem_not_below :  {ctx: TCtx}
                     -> {a,f : ctx.key}
                     -> (t: Tree23 ctx a f depth)
                     -> (x: ctx.key)
                     -> Elem x t
                     -> Not (ctx.rel x a)
lemma_elem_not_below t x elemt x_lt_a with (lemma_elem_low_lte t elemt)
  lemma_elem_not_below t x elemt x_lt_a | EQ' a_eq_x = irrefl (sym a_eq_x) x_lt_a
  lemma_elem_not_below t x elemt x_lt_a | LT' a_lt_x = asym x_lt_a a_lt_x

||| Elements of trees are not above its high bound
%hint
export
0 lemma_elem_not_above :  {ctx: TCtx}
                     -> {a,f : ctx.key}
                     -> (t: Tree23 ctx a f depth)
                     -> (x: ctx.key)
                     -> Elem x t
                     -> Not (ctx.rel f x)
lemma_elem_not_above t x elemt f_lt_x with (lemma_elem_lte_high t elemt)
  lemma_elem_not_above t x elemt f_lt_x | EQ' x_eq_f = irrefl (sym x_eq_f) f_lt_x
  lemma_elem_not_above t x elemt f_lt_x | LT' x_lt_f = asym x_lt_f f_lt_x

||| Values below the low bound of a tree are not elements of the tree
%hint
export
0 lemma_too_low :  {ctx: TCtx}
              -> {a,f,x : ctx.key}
              -> (t: Tree23 ctx a f depth)
              -> ctx.rel x a
              -> Not (Elem x t)
lemma_too_low t y z = lemma_elem_not_below t x z y

||| Values above the high bound of a tree are not elements of the tree
%hint
export
0 lemma_too_high :  {ctx: TCtx}
               -> {a,f,x : ctx.key}
               -> (t: Tree23 ctx a f depth)
               -> ctx.rel f x
               -> Not (Elem x t)
lemma_too_high t y z = lemma_elem_not_above t x z y


||| Values in neither B2 branch are not in the tree 
%hint
export
0 lemma_b2_not_in_lr :  {ctx : TCtx}
                   -> {a,b,c,d : ctx.key}
                   -> {l: Tree23 ctx a b depth}
                   -> {r: Tree23 ctx c d depth}
                   -> {b_lt_c: ctx.rel b c}
                   -> {x: ctx.key}
                   -> Not (Elem x l)
                   -> Not (Elem x r)
                   -> Not (Elem x (Branch2 l r))
lemma_b2_not_in_lr notinl notinr (InB2L y) = notinl y
lemma_b2_not_in_lr notinl notinr (InB2R y) = notinr y

||| Values in neither B3 branch are not in the tree 
%hint
export
0 lemma_b3_not_in_lmr :  {ctx : TCtx}
                    -> {a,b,c,d,e,f : ctx.key}
                    -> {l: Tree23 ctx a b depth}
                    -> {m: Tree23 ctx c d depth}
                    -> {r: Tree23 ctx e f depth}
                    -> {b_lt_c: ctx.rel b c}
                    -> {d_lt_e: ctx.rel d e}
                    -> {x: ctx.key}
                    -> Not (Elem x l)
                    -> Not (Elem x m)
                    -> Not (Elem x r)
                    -> Not (Elem x (Branch3 l m r))
lemma_b3_not_in_lmr notinl notinm notinr (InB3L y) = notinl y
lemma_b3_not_in_lmr notinl notinm notinr (InB3M y) = notinm y
lemma_b3_not_in_lmr notinl notinm notinr (InB3R y) = notinr y
