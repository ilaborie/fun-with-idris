module Data.Tree23.DecElem

import Data.Tree23.Types
import Data.Tree23.Lemmas
import Control.Relation
import Decidable.Order.Strict


||| we use the order information to look only where needed.
public export
decElem :  {ctx : TCtx}
        -> {a, f: ctx.key}
        -> (x: ctx.key)
        -> (t: Tree23 ctx a f depth)
        -> Dec (Elem x t)
decElem x (Leaf y v) with (order @{ctx.ord} x y)
  decElem x t@(Leaf x v) | DecEQ Refl = Yes InLeaf
  decElem x t@(Leaf y v) | DecLT sxy = No (\elem =>
    let xeqy = lemma_elem_leaf_eq elem
    in irrefl xeqy sxy)
  decElem x t@(Leaf y v) | DecGT syx = No (\elem =>
    let xeqy = lemma_elem_leaf_eq elem
    in irrefl (sym xeqy) syx)
decElem x (Branch2 {a} {b} {c} {d=f} l r {r0}) with (order @{ctx.ord} x b)
  decElem x (Branch2 {a} {b=x} {c} {d=f} l r {r0}) | DecEQ Refl = Yes (InB2L $ elemHigh l)
  decElem x (Branch2 {a} {b} {c} {d=f} l r {r0})   | DecLT x_lt_b with (decElem x l)
    decElem x (Branch2 {a} {b} {c} {d=f} l r {r0}) | DecLT x_lt_b | Yes eleml = Yes (InB2L eleml)
    decElem x (Branch2 {a} {b} {c} {d=f} l r {r0}) | DecLT x_lt_b | No noteleml = 
      No (\elemt => void $ lemma_b2_not_in_lr noteleml notelemr elemt)
      where
      0 notelemr : Not (Elem x r)
      notelemr = lemma_too_low r (trans {x=x} {y=b} {z=c} x_lt_b r0)
  decElem x (Branch2 {a} {b} {c} {d=f} l r {r0}) | DecGT b_lt_x with (decElem x r)
    decElem x (Branch2 {a} {b} {c} {d=f} l r {r0}) | DecGT b_lt_x | Yes elemr = Yes (InB2R elemr)
    decElem x (Branch2 {a} {b} {c} {d=f} l r {r0}) | DecGT b_lt_x | No notelemr = 
      No (\elemt => void $ lemma_b2_not_in_lr (lemma_too_high l b_lt_x) notelemr elemt)

decElem x (Branch3 {b} {c} {d} {e} l m r {r1=b_lt_c} {r2=d_lt_e}) with (order @{ctx.ord} x b)
  decElem x (Branch3 {b=x} {c} {d} {e} l m r {r1=b_lt_c} {r2=d_lt_e}) | DecEQ Refl = Yes (InB3L (elemHigh l))
  decElem x (Branch3 {b} {c} {d} {e} l m r {r1=b_lt_c} {r2=d_lt_e})   | DecLT x_lt_b with (decElem x l)
    decElem x (Branch3 {b} {c} {d} {e} l m r {r1=b_lt_c} {r2=d_lt_e})   | DecLT x_lt_b | Yes eleml = Yes (InB3L eleml)
    decElem x (Branch3 {b} {c} {d} {e} l m r {r1=b_lt_c} {r2=d_lt_e})   | DecLT x_lt_b | No noteleml =
      No (\elemt => void $ lemma_b3_not_in_lmr noteleml notelemm notelemr elemt)
      where 
      0 notelemm : Not (Elem x m)
      notelemm = lemma_too_low m (trans {x} x_lt_b b_lt_c)
      0 notelemr : Not (Elem x r)
      notelemr with (tree_lte_bounds m)
        notelemr | EQ' c_eq_d = lemma_too_low r (trans {x} x_lt_b (trans {x=b} b_lt_c c_lt_e))
          where
          c_lt_e : ctx.rel c e
          c_lt_e = rewrite c_eq_d in d_lt_e
        notelemr | LT' c_lt_d = lemma_too_low r (trans {x} x_lt_b (trans {x=b} b_lt_c (trans {x=c} c_lt_d d_lt_e)))
      
  decElem x (Branch3 {b} {c} {d} {e} l m r {r1=b_lt_c} {r2=d_lt_e}) | DecGT b_lt_x with (order @{ctx.ord} x d)
    decElem x (Branch3 {b} {c} {d=x} {e} l m r {r1=b_lt_c} {r2=d_lt_e}) | DecGT b_lt_x | DecEQ Refl = Yes (InB3M (elemHigh m))
    decElem x (Branch3 {b} {c} {d} {e} l m r {r1=b_lt_c} {r2=d_lt_e}) | DecGT b_lt_x | DecLT x_lt_d with (decElem x m)
      decElem x (Branch3 {b} {c} {d} {e} l m r {r1=b_lt_c} {r2=d_lt_e}) | DecGT b_lt_x | DecLT x_lt_d | Yes elemm = Yes (InB3M elemm)
      decElem x (Branch3 {b} {c} {d} {e} l m r {r1=b_lt_c} {r2=d_lt_e}) | DecGT b_lt_x | DecLT x_lt_d | No notelemm =
        No (\elemt => void $ lemma_b3_not_in_lmr noteleml notelemm notelemr elemt)
        where
        0 noteleml : Not (Elem x l)
        noteleml eleml = lemma_too_high l b_lt_x eleml
        0 notelemr : Not (Elem x r)
        notelemr elemr = lemma_too_low r (trans {x} x_lt_d d_lt_e) elemr
    decElem x (Branch3 {b} {c} {d} {e} l m r {r1=b_lt_c} {r2=d_lt_e}) | DecGT b_lt_x | DecGT d_lt_x with (decElem x r)
      decElem x (Branch3 {b} {c} {d} {e} l m r {r1=b_lt_c} {r2=d_lt_e}) | DecGT b_lt_x | DecGT d_lt_x | Yes elemr = Yes (InB3R elemr)
      decElem x (Branch3 {b} {c} {d} {e} l m r {r1=b_lt_c} {r2=d_lt_e}) | DecGT b_lt_x | DecGT d_lt_x | No notelemr =
        No (\elemt => void $ lemma_b3_not_in_lmr noteleml notelemm notelemr elemt)
        where
        0 noteleml : Not (Elem x l)
        noteleml eleml = lemma_too_high l b_lt_x eleml
        0 notelemm : Not (Elem x m)
        notelemm elemm = lemma_too_high m d_lt_x elemm

