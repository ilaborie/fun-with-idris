module Data.Tree23.Bounds

import Decidable.Order.Strict

import public Data.Tree23.Types
import public Data.Tree23.Lemmas
import public Data.Tree23.DecElem
import public Data.Tree23.Merge

%default total

public export
record TreeBounds (ctx:TCtx) where
  constructor TB
  low: ctx.key
  hih: ctx.key
  0 lte: LTE' ctx.key ctx.rel low hih
  0 dep: Nat

public export
bounds : {ctx: TCtx} -> {a,b: ctx.key}
       -> Tree23 ctx a b depth
       -> TreeBounds ctx
bounds t = TB a b (tree_lte_bounds t) depth

public export
data BoundsRel : {ctx: TCtx}
               -> {a,f: ctx.key}
               -> (k: ctx.key)
               -> (t: Tree23 ctx a f depth)
               -> Type where
  LeafBelow:  {ctx: TCtx}
           -> {k,a: ctx.key}
           -> {v: ctx.val a}
           -> (0 k_lt_a : ctx.rel k a)
           -> BoundsRel {ctx} {a=a,f=a} k (Leaf a v)
  LeafSame:   {ctx: TCtx}
           -> {k,a: ctx.key}
           -> {v: ctx.val a}
           -> (k_eq_a : k = a)
           -> BoundsRel {ctx} {a=a,f=a} k (Leaf a v)
  LeafAbove:  {ctx: TCtx}
           -> {k,a: ctx.key}
           -> {v: ctx.val a}
           -> (0 a_lt_k : ctx.rel a k)
           -> BoundsRel {ctx} {a=a,f=a} k (Leaf a v)
  B2Below:    {ctx: TCtx}
           -> {k,a,b,c,f: ctx.key} -> {0 r0 : ctx.rel b c}
           -> {l: Tree23 ctx a b depth}
           -> {r: Tree23 ctx c f depth}
           -> (0 k_lt_a : ctx.rel k a)
           -> BoundsRel k (Branch2 {a,b,c,d=f,r0} l r)
  B2Above:    {ctx: TCtx}
           -> {k,a,b,c,f: ctx.key} -> {0 r0 : ctx.rel b c}
           -> {l: Tree23 ctx a b depth}
           -> {r: Tree23 ctx c f depth}
           -> (0 f_lt_k : ctx.rel f k)
           -> BoundsRel k (Branch2 {a,b,c,d=f,r0} l r)
  B2LowL:     {ctx: TCtx}
           -> {k,a,b,c,f: ctx.key} -> {0 r0 : ctx.rel b c}
           -> {l: Tree23 ctx a b depth}
           -> {r: Tree23 ctx c f depth}
           -> (a_eq_k : a = k)
           -> BoundsRel k (Branch2 {a,b,c,d=f,r0} l r)
  B2HighL:     {ctx: TCtx}
           -> {k,a,b,c,f: ctx.key} -> {0 r0 : ctx.rel b c}
           -> {l: Tree23 ctx a b depth}
           -> {r: Tree23 ctx c f depth}
           -> (b_eq_k : b = k)
           -> BoundsRel k (Branch2 {a,b,c,d=f,r0} l r)
  B2LowR:     {ctx: TCtx}
           -> {k,a,b,c,f: ctx.key} -> {0 r0 : ctx.rel b c}
           -> {l: Tree23 ctx a b depth}
           -> {r: Tree23 ctx c f depth}
           -> (c_eq_k : c = k)
           -> BoundsRel k (Branch2 {a,b,c,d=f,r0} l r)
  B2HighR:     {ctx: TCtx}
           -> {k,a,b,c,f: ctx.key} -> {0 r0 : ctx.rel b c}
           -> {l: Tree23 ctx a b depth}
           -> {r: Tree23 ctx c f depth}
           -> (f_eq_k : f = k)
           -> BoundsRel k (Branch2 {a,b,c,d=f,r0} l r)
  B2InL:      {ctx: TCtx}
           -> {k,a,b,c,f: ctx.key} -> {0 r0 : ctx.rel b c}
           -> {l: Tree23 ctx a b depth}
           -> {r: Tree23 ctx c f depth}
           -> (0 a_lt_k : ctx.rel a k)-> (0 k_lt_b : ctx.rel k b)
           -> BoundsRel k (Branch2 {a,b,c,d=f,r0} l r)
  B2InR:      {ctx: TCtx}
           -> {k,a,b,c,f: ctx.key} -> {0 r0 : ctx.rel b c}
           -> {l: Tree23 ctx a b depth}
           -> {r: Tree23 ctx c f depth}
           -> (0 c_lt_k : ctx.rel c k)-> (0 k_lt_f : ctx.rel k f)
           -> BoundsRel k (Branch2 {a,b,c,d=f,r0} l r)
  B2BetweenLR:{ctx: TCtx}
           -> {k,a,b,c,f: ctx.key} -> {0 r0 : ctx.rel b c}
           -> {l: Tree23 ctx a b depth}
           -> {r: Tree23 ctx c f depth}
           -> (0 b_lt_k : ctx.rel b k)-> (0 k_lt_c : ctx.rel k c)
           -> BoundsRel k (Branch2 {a,b,c,d=f,r0} l r)
  B3Below:    {ctx: TCtx}
           -> {k,a,b,c,d,e,f: ctx.key} 
           -> {0 r1 : ctx.rel b c} -> {0 r2: ctx.rel d e}
           -> {l: Tree23 ctx a b depth}
           -> {m: Tree23 ctx c d depth}
           -> {r: Tree23 ctx e f depth}
           -> (0 k_lt_a : ctx.rel k a)
           -> BoundsRel k (Branch3 {a,b,c,d,e,f,r1,r2} l m r)
  B3Above:    {ctx: TCtx}
           -> {k,a,b,c,d,e,f: ctx.key} 
           -> {0 r1 : ctx.rel b c} -> {0 r2: ctx.rel d e}
           -> {l: Tree23 ctx a b depth}
           -> {m: Tree23 ctx c d depth}
           -> {r: Tree23 ctx e f depth}
           -> (0 f_lt_k : ctx.rel f k)
           -> BoundsRel k (Branch3 {a,b,c,d,e,f,r1,r2} l m r)
  B3LowL:     {ctx: TCtx}
           -> {k,a,b,c,d,e,f: ctx.key} 
           -> {0 r1 : ctx.rel b c} -> {0 r2: ctx.rel d e}
           -> {l: Tree23 ctx a b depth}
           -> {m: Tree23 ctx c d depth}
           -> {r: Tree23 ctx e f depth}
           -> (a_eq_k : a = k)
           -> BoundsRel k (Branch3 {a,b,c,d,e,f,r1,r2} l m r)
  B3HighL:     {ctx: TCtx}
           -> {k,a,b,c,d,e,f: ctx.key} 
           -> {0 r1 : ctx.rel b c} -> {0 r2: ctx.rel d e}
           -> {l: Tree23 ctx a b depth}
           -> {m: Tree23 ctx c d depth}
           -> {r: Tree23 ctx e f depth}
           -> (b_eq_k : b = k)
           -> BoundsRel k (Branch3 {a,b,c,d,e,f,r1,r2} l m r)
  B3LowM:     {ctx: TCtx}
           -> {k,a,b,c,d,e,f: ctx.key} 
           -> {0 r1 : ctx.rel b c} -> {0 r2: ctx.rel d e}
           -> {l: Tree23 ctx a b depth}
           -> {m: Tree23 ctx c d depth}
           -> {r: Tree23 ctx e f depth}
           -> (c_eq_k : c = k)
           -> BoundsRel k (Branch3 {a,b,c,d,e,f,r1,r2} l m r)
  B3HighM:     {ctx: TCtx}
           -> {k,a,b,c,d,e,f: ctx.key} 
           -> {0 r1 : ctx.rel b c} -> {0 r2: ctx.rel d e}
           -> {l: Tree23 ctx a b depth}
           -> {m: Tree23 ctx c d depth}
           -> {r: Tree23 ctx e f depth}
           -> (f_eq_k : d = k)
           -> BoundsRel k (Branch3 {a,b,c,d,e,f,r1,r2} l m r)
  B3LowR:     {ctx: TCtx}
           -> {k,a,b,c,d,e,f: ctx.key} 
           -> {0 r1 : ctx.rel b c} -> {0 r2: ctx.rel d e}
           -> {l: Tree23 ctx a b depth}
           -> {m: Tree23 ctx c d depth}
           -> {r: Tree23 ctx e f depth}
           -> (c_eq_k : e = k)
           -> BoundsRel k (Branch3 {a,b,c,d,e,f,r1,r2} l m r)
  B3HighR:     {ctx: TCtx}
           -> {k,a,b,c,d,e,f: ctx.key} 
           -> {0 r1 : ctx.rel b c} -> {0 r2: ctx.rel d e}
           -> {l: Tree23 ctx a b depth}
           -> {m: Tree23 ctx c d depth}
           -> {r: Tree23 ctx e f depth}
           -> (f_eq_k : f = k)
           -> BoundsRel k (Branch3 {a,b,c,d,e,f,r1,r2} l m r)

  B3InL:      {ctx: TCtx}
           -> {k,a,b,c,d,e,f: ctx.key} 
           -> {0 r1 : ctx.rel b c} -> {0 r2: ctx.rel d e}
           -> {l: Tree23 ctx a b depth}
           -> {m: Tree23 ctx c d depth}
           -> {r: Tree23 ctx e f depth}
           -> (0 a_lt_k : ctx.rel a k)-> (0 k_lt_b : ctx.rel k b)
           -> BoundsRel k (Branch3 {a,b,c,d,e,f,r1,r2} l m r)
  B3InM:      {ctx: TCtx}
           -> {k,a,b,c,d,e,f: ctx.key} 
           -> {0 r1 : ctx.rel b c} -> {0 r2: ctx.rel d e}
           -> {l: Tree23 ctx a b depth}
           -> {m: Tree23 ctx c d depth}
           -> {r: Tree23 ctx e f depth}
           -> (0 c_lt_k : ctx.rel c k)-> (0 k_lt_f : ctx.rel k d)
           -> BoundsRel k (Branch3 {a,b,c,d,e,f,r1,r2} l m r)
  B3InR:      {ctx: TCtx}
           -> {k,a,b,c,d,e,f: ctx.key} 
           -> {0 r1 : ctx.rel b c} -> {0 r2: ctx.rel d e}
           -> {l: Tree23 ctx a b depth}
           -> {m: Tree23 ctx c d depth}
           -> {r: Tree23 ctx e f depth}
           -> (0 e_lt_k : ctx.rel e k)-> (0 k_lt_f : ctx.rel k f)
           -> BoundsRel k (Branch3 {a,b,c,d,e,f,r1,r2} l m r)

  B3BetweenLM:{ctx: TCtx}
           -> {k,a,b,c,d,e,f: ctx.key} 
           -> {0 r1 : ctx.rel b c} -> {0 r2: ctx.rel d e}
           -> {l: Tree23 ctx a b depth}
           -> {m: Tree23 ctx c d depth}
           -> {r: Tree23 ctx e f depth}
           -> (0 b_lt_k : ctx.rel b k) -> (0 k_lt_c : ctx.rel k c)
           -> BoundsRel k (Branch3 {a,b,c,d,e,f,r1,r2} l m r)
  B3BetweenMR:{ctx: TCtx}
           -> {k,a,b,c,d,e,f: ctx.key} 
           -> {0 r1 : ctx.rel b c} -> {0 r2: ctx.rel d e}
           -> {l: Tree23 ctx a b depth}
           -> {m: Tree23 ctx c d depth}
           -> {r: Tree23 ctx e f depth}
           -> (0 d_lt_k : ctx.rel d k) -> (0 k_lt_e : ctx.rel k e)
           -> BoundsRel k (Branch3 {a,b,c,d,e,f,r1,r2} l m r)

public export
boundsRel :  {ctx:TCtx} -> {a,f: ctx.key}
             -> (k : ctx.key)
             -> (t: Tree23 ctx a f depth)
             -> BoundsRel k t
boundsRel k (Leaf a v) with (order @{ctx.ord} k a)
  boundsRel k (Leaf k v) | DecEQ Refl = LeafSame Refl
  boundsRel k (Leaf a v) | DecLT klta = LeafBelow klta
  boundsRel k (Leaf a v) | DecGT altk = LeafAbove altk
boundsRel k (Branch2 {a,b,c,d=f,r0} l r) with (order @{ctx.ord} k b)
  boundsRel k (Branch2 {a,b=k,c,d=f,r0} l r) | DecEQ Refl = B2HighL Refl
  boundsRel k (Branch2 {a,b,c,d=f,r0} l r) | DecLT kltb with (order @{ctx.ord} k a)
    boundsRel k (Branch2 {a=k,b,c,d=f,r0} l r) | DecLT kltb | DecEQ Refl = B2LowL Refl
    boundsRel k (Branch2 {a  ,b,c,d=f,r0} l r) | DecLT kltb | DecLT klta = B2Below klta
    boundsRel k (Branch2 {a  ,b,c,d=f,r0} l r) | DecLT kltb | DecGT altk = B2InL altk kltb
  boundsRel k (Branch2 {a,b,c,d=f,r0} l r) | DecGT bltk with (order @{ctx.ord} k c)
    boundsRel k (Branch2 {a,b,c=k,d=f,r0} l r) | DecGT bltk | DecEQ Refl = B2LowR Refl
    boundsRel k (Branch2 {a,b,c  ,d=f,r0} l r) | DecGT bltk | DecLT kltc = B2BetweenLR bltk kltc
    boundsRel k (Branch2 {a,b,c  ,d=f,r0} l r) | DecGT bltk | DecGT cltk with (order @{ctx.ord} k f)
      boundsRel k (Branch2 {a,b,c,d=k,r0} l r) | DecGT bltk | DecGT cltk | DecEQ Refl = B2HighR Refl
      boundsRel k (Branch2 {a,b,c,d=f,r0} l r) | DecGT bltk | DecGT cltk | DecLT kltf = B2InR cltk kltf
      boundsRel k (Branch2 {a,b,c,d=f,r0} l r) | DecGT bltk | DecGT cltk | DecGT fltk = B2Above fltk
boundsRel k (Branch3 {a,b,c,d,e,f,r1,r2} l m r) with (order @{ctx.ord} k c)
  boundsRel k (Branch3 {a,b,c=k,d,e,f,r1,r2} l m r) | DecEQ Refl = B3LowM Refl
  boundsRel k (Branch3 {a,b,c  ,d,e,f,r1,r2} l m r) | DecLT kltc with (order @{ctx.ord} k b)
    boundsRel k (Branch3 {a,b=k,c,d,e,f,r1,r2} l m r) | DecLT kltc | DecEQ Refl = B3HighL Refl
    boundsRel k (Branch3 {a,b  ,c,d,e,f,r1,r2} l m r) | DecLT kltc | DecLT kltb with (order @{ctx.ord} k a)
      boundsRel k (Branch3 {a=k,b,c,d,e,f,r1,r2} l m r) | DecLT kltc | DecLT kltb | DecEQ Refl = B3LowL Refl
      boundsRel k (Branch3 {a  ,b,c,d,e,f,r1,r2} l m r) | DecLT kltc | DecLT kltb | DecLT klta = B3Below klta
      boundsRel k (Branch3 {a  ,b,c,d,e,f,r1,r2} l m r) | DecLT kltc | DecLT kltb | DecGT altk = B3InL altk kltb
    boundsRel k (Branch3 {a,b  ,c,d,e,f,r1,r2} l m r) | DecLT kltc | DecGT bltk = B3BetweenLM bltk kltc
  boundsRel k (Branch3 {a,b,c  ,d,e,f,r1,r2} l m r) | DecGT cltk with (order @{ctx.ord} k e)
    boundsRel k (Branch3 {a,b,c,d,e=k,f,r1,r2} l m r) | DecGT cltk | DecEQ Refl = B3LowR Refl
    boundsRel k (Branch3 {a,b,c,d,e  ,f,r1,r2} l m r) | DecGT cltk | DecLT klte with (order @{ctx.ord} k d)
      boundsRel k (Branch3 {a,b,c,d=k,e,f,r1,r2} l m r) | DecGT cltk | DecLT klte | DecEQ Refl = B3HighM Refl
      boundsRel k (Branch3 {a,b,c,d  ,e,f,r1,r2} l m r) | DecGT cltk | DecLT klte | DecLT kltd = B3InM cltk kltd
      boundsRel k (Branch3 {a,b,c,d  ,e,f,r1,r2} l m r) | DecGT cltk | DecLT klte | DecGT dltk = B3BetweenMR dltk klte 
    boundsRel k (Branch3 {a,b,c,d,e  ,f,r1,r2} l m r) | DecGT cltk | DecGT eltk with (order @{ctx.ord} k f)
      boundsRel k (Branch3 {a,b,c,d,e,f=k,r1,r2} l m r) | DecGT cltk | DecGT eltk | DecEQ Refl = B3HighR Refl
      boundsRel k (Branch3 {a,b,c,d,e,f  ,r1,r2} l m r) | DecGT cltk | DecGT eltk | DecLT kltf = B3InR eltk kltf
      boundsRel k (Branch3 {a,b,c,d,e,f  ,r1,r2} l m r) | DecGT cltk | DecGT eltk | DecGT fltk = B3Above fltk
