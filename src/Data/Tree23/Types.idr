module Data.Tree23.Types

import Control.Relation
import Decidable.Order.Strict


public export
record TCtx where
  key: Type
  val: key -> Type
  rel: key -> key -> Type
  ord: StrictOrdered key rel
  irr: Irreflexive key rel
  tra: Transitive key rel

public export
data Tree23:  (ctx: TCtx)
           -> (low, high: ctx.key)
           -> (0 depth: Nat)
           -> Type where
            
  Leaf     :  {ctx: TCtx}
           -> (k : ctx.key)
           -> (v: (ctx.val) k)
           -> Tree23 ctx k k 0

  Branch2 :  {ctx: TCtx}
          -> {a, b, c, d: ctx.key}
          -> Tree23 ctx a b depth
          -> {auto 0 r0: ctx.rel b c}
          -> Tree23 ctx c d depth
          -> Tree23 ctx a d (S depth)

  Branch3 :  {ctx: TCtx}
          -> {a, b, c, d, e, f: ctx.key}
          -> Tree23 ctx a b depth
          -> {auto 0 r1 : ctx.rel b c }
          -> Tree23 ctx c d depth
          -> {auto 0 r2 : ctx.rel d e }
          -> Tree23 ctx e f depth
          -> Tree23 ctx a f (S depth)

public export
data Elem :  {ctx : TCtx}
          -> {k1, k2: ctx.key}
          -> (x : ctx.key)
          -> (t : Tree23 ctx k1 k2 depth)
          -> Type where

  InLeaf : Elem x (Leaf x v)

  InB2L :  {l : Tree23 ctx a b depth} 
        -> {r : Tree23 ctx c d depth}
        -> {auto 0 r0 : ctx.rel b c}
        -> Elem x l
        -> Elem x (Branch2 l r)
  InB2R :  {l : Tree23 ctx a b depth} 
        -> {r : Tree23 ctx c d depth}
        -> {auto 0 r0 : ctx.rel b c} 
        -> Elem x r -> Elem x (Branch2 l r)

  InB3L :  {l : Tree23 ctx a b depth} 
        -> {m : Tree23 ctx c d depth}
        -> {r : Tree23 ctx e f depth}
        -> {auto 0 r1 : ctx.rel b c}
        -> {auto 0 r2 : ctx.rel d e} 
        -> Elem x l
        -> Elem x (Branch3 l m r)
  InB3M :  {l : Tree23 ctx a b depth} 
        -> {m : Tree23 ctx c d depth}
        -> {r : Tree23 ctx e f depth}
        -> {auto 0 r1 : ctx.rel b c}
        -> {auto 0 r2 : ctx.rel d e} 
        -> Elem x m
        -> Elem x (Branch3 l m r)
  InB3R :  {l : Tree23 ctx a b depth} 
        -> {m : Tree23 ctx c d depth}
        -> {r : Tree23 ctx e f depth}
        -> {auto 0 r1 : ctx.rel b c}
        -> {auto 0 r2 : ctx.rel d e} 
        -> Elem x r
        -> Elem x (Branch3 l m r)

public export
data LTE' : (key : Type) -> (rel : key -> key -> Type) -> (a,b : key) -> Type where
  LT' :  {key : Type}
      -> {rel : key -> key -> Type} 
      -> {b,c : key}
      -> rel b c
      -> LTE' key rel b c
  EQ' :  {key : Type}
      -> {rel : key -> key -> Type}
      -> {b,c : key}
      -> b = c
      -> LTE' key rel b c


