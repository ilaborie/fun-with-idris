module Data.Tree23.Insert

import public Data.Tree23.Types
import public Data.Tree23.Lemmas
import public Data.Tree23.DecElem
import public Data.Tree23.Merge
import public Data.Tree23.Bounds

%default total

export
treeReplace :  {ctx: TCtx} -> {a,f: ctx.key} -> {0 depth: Nat}
            -> (k: ctx.key)
            -> (v: ctx.val k)
            -> (t: Tree23 ctx a f depth)
            -> (elem: Elem k t)
            -> (t' : Tree23 ctx a f depth ** Elem k t')
treeReplace k v (Leaf k y) InLeaf = (Leaf k v ** InLeaf)
treeReplace k v (Branch2 l r)   e@(InB2L eleml) = let s = treeReplace k v l eleml in (Branch2 (s.fst) r ** InB2L (s.snd)) 
treeReplace k v (Branch2 l r)   e@(InB2R elemr) = let s = treeReplace k v r elemr in (Branch2 l (s.fst) ** InB2R (s.snd)) 
treeReplace k v (Branch3 l m r) e@(InB3L eleml) = let s = treeReplace k v l eleml in (Branch3 (s.fst) m r ** InB3L (s.snd)) 
treeReplace k v (Branch3 l m r) e@(InB3M elemm) = let s = treeReplace k v m elemm in (Branch3 l (s.fst) r ** InB3M (s.snd)) 
treeReplace k v (Branch3 l m r) e@(InB3R elemr) = let s = treeReplace k v r elemr in (Branch3 l m (s.fst) ** InB3R (s.snd)) 

public export
data InsertedBounds :  {ctx: TCtx}
                    -> {a,f: ctx.key}
                    -> (k : ctx.key)
                    -> (0 t : Tree23 ctx a f depth)
                    -> Type where

  SameBounds:  {ctx: TCtx}
            -> {a,f: ctx.key}
            -> (k : ctx.key)
            -> (0 t : Tree23 ctx a f depth)
            -> (t': Tree23 ctx a f depth)
            -> (Elem k t')
            -> InsertedBounds {ctx,a,f} k t

  SameBoundsDeeper:  {ctx: TCtx}
            -> {a,f: ctx.key}
            -> (k : ctx.key)
            -> (0 t : Tree23 ctx a f depth)
            -> (t': Tree23 ctx a f (S depth))
            -> (Elem k t')
            -> InsertedBounds {ctx,a,f} k t

  LowerBounds:  {ctx: TCtx}
            -> {a,f,a': ctx.key}
            -> (0 a'lta : ctx.rel a' a)
            -> (0 t : Tree23 ctx a  f depth)
            -> (t': Tree23 ctx a' f depth)
            -> (Elem a' t')
            -> InsertedBounds {ctx,a,f} a' t

  LowerBoundsDeeper:  {ctx: TCtx}
            -> {a,f,a': ctx.key}
            -> (0 a'lta : ctx.rel a' a)
            -> (0 t : Tree23 ctx a  f depth)
            -> (t': Tree23 ctx a' f (S depth))
            -> (Elem a' t')
            -> InsertedBounds {ctx,a,f} a' t

  HigherBounds:  {ctx: TCtx}
            -> {a,f,f': ctx.key}
            -> (0 fltf' : ctx.rel f f')
            -> (0 t : Tree23 ctx a f  depth)
            -> (t': Tree23 ctx a f' depth)
            -> (Elem f' t')
            -> InsertedBounds {ctx,a,f} f' t

  HigherBoundsDeeper:  {ctx: TCtx}
            -> {a,f,f': ctx.key}
            -> (0 fltf' : ctx.rel f f')
            -> (0 t : Tree23 ctx a f  depth)
            -> (t': Tree23 ctx a f' (S depth))
            -> (Elem f' t')
            -> InsertedBounds {ctx,a,f} f' t

export
treeInsert :  {ctx:TCtx}
           -> {a,f: ctx.key}
           -> {0 depth: Nat}
           -> (k: ctx.key)
           -> (v: ctx.val k)
           -> (t: Tree23 ctx a f depth)
           -> InsertedBounds {ctx} k t
treeInsert k v t with (boundsRel k t)
    
  treeInsert k v (Leaf k' v') | (LeafBelow k_lt_a) =
    LowerBoundsDeeper k_lt_a _ (Branch2 (Leaf k v) (Leaf k' v')) (InB2L InLeaf)
  treeInsert k v (Leaf k  v') | (LeafSame Refl) =
    SameBounds k _ (Leaf k v) InLeaf
  treeInsert k v (Leaf k' v') | (LeafAbove a_lt_k) =
    HigherBoundsDeeper a_lt_k _ (Branch2 (Leaf k' v') (Leaf k v)) (InB2R InLeaf)

  treeInsert k v (Branch2 l r) | (B2Below k_lt_a) with (treeInsert k v l)
    treeInsert k v (Branch2 l r) | (B2Below k_lt_a) | (SameBounds k l t' y) =
      void (lemma_too_low t' k_lt_a y)
    treeInsert k v (Branch2 l r) | (B2Below k_lt_a) | (SameBoundsDeeper k l t' y) = 
      void (lemma_too_low t' k_lt_a y)
    treeInsert b v (Branch2 l r) | (B2Below k_lt_a) | (LowerBounds a'lta l t e) = 
      LowerBounds k_lt_a _ (Branch2 t r) (InB2L e)
    treeInsert k v (Branch2 l r) | (B2Below k_lt_a) | (LowerBoundsDeeper a'lta l (Branch2 il ir) (InB2L e)) =
      LowerBounds k_lt_a _ (Branch3 il ir r) (InB3L e) 
    treeInsert k v (Branch2 l r) | (B2Below k_lt_a) | (LowerBoundsDeeper a'lta l (Branch2 {r0} il ir) (InB2R e)) =
      void $ lemma_too_low ir (il ~<! r0) e
    treeInsert k v (Branch2 l r) | (B2Below k_lt_a) | (LowerBoundsDeeper a'lta l (Branch3 il im ir) (InB3L e)) =
      LowerBoundsDeeper k_lt_a _ (Branch2 (Branch2 il im) (Branch2 ir r)) (InB2L (InB2L e))
    treeInsert k v (Branch2 l r) | (B2Below k_lt_a) | (LowerBoundsDeeper a'lta l (Branch3 {r1} il im ir) (InB3M e)) =
      void $ lemma_too_low im (il ~<! r1) e
    treeInsert k v (Branch2 l r) | (B2Below k_lt_a) | (LowerBoundsDeeper a'lta l (Branch3 {r1,r2} il im ir) (InB3R e)) =
      void $ lemma_too_low ir (il ~<! (r1 !<~ im) !<! r2) e
    treeInsert k v (Branch2 l r) | (B2Below k_lt_a) | (HigherBounds fltf' l t' x) = 
      void $ asym fltf' (k_lt_a !<~ l)
    treeInsert k v (Branch2 l r) | (B2Below k_lt_a) | (HigherBoundsDeeper fltf' l t' x) = 
      void $ asym fltf' (k_lt_a !<~ l)

  treeInsert k v (Branch2 l r) | (B2Above f_lt_k) with (treeInsert k v r)
    treeInsert k v (Branch2 l r) | (B2Above f_lt_k) | (SameBounds k r t' y) =
      void (lemma_too_high t' f_lt_k y)
    treeInsert k v (Branch2 l r) | (B2Above f_lt_k) | (SameBoundsDeeper k r t' y) =
      void (lemma_too_high t' f_lt_k y)
    treeInsert k v (Branch2 l r) | (B2Above f_lt_k) | (LowerBounds a'lta r t' x) =
      void $ asym a'lta (trans_tree_lt r f_lt_k)
    treeInsert k v (Branch2 l r) | (B2Above f_lt_k) | (LowerBoundsDeeper a'lta r t' x) =
      void $ asym a'lta (trans_tree_lt r f_lt_k)
    treeInsert k v (Branch2 l r) | (B2Above f_lt_k) | (HigherBounds fltf' r t' x) =
      HigherBounds f_lt_k _ (Branch2 l t') (InB2R x) 
    treeInsert k v (Branch2 l r) | (B2Above f_lt_k) | (HigherBoundsDeeper fltf' r (Branch2 {b=ib,c=ic,r0} x y) (InB2L e)) =
      void $ lemma_too_high x (trans_lt_tree r0 y) e
    treeInsert k v (Branch2 l r) | (B2Above f_lt_k) | (HigherBoundsDeeper fltf' r (Branch2 x y) (InB2R e)) =
      HigherBounds f_lt_k _ (Branch3 l x y) (InB3R e)
    treeInsert k v (Branch2 l r) | (B2Above f_lt_k) | (HigherBoundsDeeper fltf' r (Branch3 {b=ib,c=ic,d=id,e=ie,r1,r2} x y z) (InB3L e)) =
      void $ lemma_too_high x (trans_lt_tree (trans {ctx} (trans_lt_tree r1 y) r2) z) e
    treeInsert k v (Branch2 l r) | (B2Above f_lt_k) | (HigherBoundsDeeper fltf' r (Branch3 {r2} x y z) (InB3M e)) =
      void $ lemma_too_high y (trans_lt_tree r2 z) e
    treeInsert k v (Branch2 l r) | (B2Above f_lt_k) | (HigherBoundsDeeper fltf' r (Branch3 x y z) (InB3R e)) =
      HigherBoundsDeeper f_lt_k _ (Branch2 (Branch2 l x) (Branch2 y z)) (InB2R (InB2R e))

  treeInsert k v (Branch2 l r) | (B2LowL Refl) = 
    let
    e = elemLow l 
    (t' ** e') = (treeReplace k v l e) in
    SameBounds k _ (Branch2 t' r) (InB2L e')

  treeInsert k v (Branch2 l r) | (B2HighL Refl) =
    let
    e = elemHigh l 
    (t' ** e') = (treeReplace k v l e) in
    SameBounds k _ (Branch2 t' r) (InB2L e')

  treeInsert k v (Branch2 l r) | (B2LowR Refl) =
    let
    e = elemLow r
    (t' ** e') = (treeReplace k v r e) in
    SameBounds k _ (Branch2 l t') (InB2R e')

  treeInsert k v (Branch2 l r) | (B2HighR Refl) =
    let
    e = elemHigh r
    (t' ** e') = (treeReplace k v r e) in
    SameBounds k _ (Branch2 l t') (InB2R e')
  
  treeInsert k v (Branch2 l r) | (B2InL a_lt_k k_lt_b) with (treeInsert k v l)
    treeInsert k v (Branch2 l r) | (B2InL a_lt_k k_lt_b) | (SameBounds k l t' e) =
      SameBounds k _ (Branch2 t' r) (InB2L e)
    treeInsert k v (Branch2 l r) | (B2InL a_lt_k k_lt_b) | (SameBoundsDeeper k l (Branch2 x y) (InB2L e)) =
      SameBounds k _ (Branch3 x y r) (InB3L e)
    treeInsert k v (Branch2 l r) | (B2InL a_lt_k k_lt_b) | (SameBoundsDeeper k l (Branch2 x y) (InB2R e)) =
      SameBounds k _ (Branch3 x y r) (InB3M e)
    treeInsert k v (Branch2 l r) | (B2InL a_lt_k k_lt_b) | (SameBoundsDeeper k l (Branch3 x y z) (InB3L e)) =
      SameBoundsDeeper k _ (Branch2 (Branch2 x y) (Branch2 z r)) (InB2L (InB2L e))
    treeInsert k v (Branch2 l r) | (B2InL a_lt_k k_lt_b) | (SameBoundsDeeper k l (Branch3 x y z) (InB3M e)) =
      SameBoundsDeeper k _ (Branch2 (Branch2 x y) (Branch2 z r)) (InB2L (InB2R e))
    treeInsert k v (Branch2 l r) | (B2InL a_lt_k k_lt_b) | (SameBoundsDeeper k l (Branch3 x y z) (InB3R e)) =
      SameBoundsDeeper k _ (Branch2 (Branch2 x y) (Branch2 z r)) (InB2R (InB2L e))
    treeInsert k v (Branch2 l r) | (B2InL a_lt_k k_lt_b) | (LowerBounds a'lta l t' x) = void $ asym a'lta a_lt_k
    treeInsert k v (Branch2 l r) | (B2InL a_lt_k k_lt_b) | (LowerBoundsDeeper a'lta l t' x) = void $ asym a'lta a_lt_k
    treeInsert k v (Branch2 l r) | (B2InL a_lt_k k_lt_b) | (HigherBounds fltf' l t' x) = void $ asym fltf' k_lt_b
    treeInsert k v (Branch2 l r) | (B2InL a_lt_k k_lt_b) | (HigherBoundsDeeper fltf' l t' x) = void $ asym fltf' k_lt_b

  treeInsert k v (Branch2 l r) | (B2InR c_lt_k k_lt_f) with (treeInsert k v r)
    treeInsert k v (Branch2 l r) | (B2InR c_lt_k k_lt_f) | (SameBounds k r t' e) =
      SameBounds k _ (Branch2 l t') (InB2R e)
    treeInsert k v (Branch2 l r) | (B2InR c_lt_k k_lt_f) | (SameBoundsDeeper k r (Branch2 x y) (InB2L e)) =
      SameBounds k _ (Branch3 l x y) (InB3M e)
    treeInsert k v (Branch2 l r) | (B2InR c_lt_k k_lt_f) | (SameBoundsDeeper k r (Branch2 x y) (InB2R e)) =
      SameBounds k _ (Branch3 l x y) (InB3R e)
    treeInsert k v (Branch2 l r) | (B2InR c_lt_k k_lt_f) | (SameBoundsDeeper k r (Branch3 x y z) (InB3L e)) =
      SameBoundsDeeper k _ (Branch2 (Branch2 l x) (Branch2 y z)) (InB2L (InB2R e))
    treeInsert k v (Branch2 l r) | (B2InR c_lt_k k_lt_f) | (SameBoundsDeeper k r (Branch3 x y z) (InB3M e)) =
      SameBoundsDeeper k _ (Branch2 (Branch2 l x) (Branch2 y z)) (InB2R (InB2L e))
    treeInsert k v (Branch2 l r) | (B2InR c_lt_k k_lt_f) | (SameBoundsDeeper k r (Branch3 x y z) (InB3R e)) =
      SameBoundsDeeper k _ (Branch2 (Branch2 l x) (Branch2 y z)) (InB2R (InB2R e))
    treeInsert k v (Branch2 l r) | (B2InR c_lt_k k_lt_f) | (LowerBounds a'lta r t' e) = void $ asym a'lta c_lt_k
    treeInsert k v (Branch2 l r) | (B2InR c_lt_k k_lt_f) | (LowerBoundsDeeper a'lta r t' e) = void $ asym a'lta c_lt_k
    treeInsert k v (Branch2 l r) | (B2InR c_lt_k k_lt_f) | (HigherBounds fltf' r t' e) = void $ asym fltf' k_lt_f
    treeInsert k v (Branch2 l r) | (B2InR c_lt_k k_lt_f) | (HigherBoundsDeeper fltf' r t' e) = void $ asym fltf' k_lt_f

  treeInsert k v (Branch2 l r) | (B2BetweenLR b_lt_k k_lt_c) with (treeInsert k v l)
    treeInsert k v (Branch2 l r) | (B2BetweenLR b_lt_k k_lt_c) | (SameBounds k l t' e) = void $ lemma_too_high t' b_lt_k e
    treeInsert k v (Branch2 l r) | (B2BetweenLR b_lt_k k_lt_c) | (SameBoundsDeeper k l t' e) = void $ lemma_too_high t' b_lt_k e
    treeInsert k v (Branch2 l r) | (B2BetweenLR b_lt_k k_lt_c) | (LowerBounds a'lta l t' e) = void $ lemma_too_high t' b_lt_k e
    treeInsert k v (Branch2 l r) | (B2BetweenLR b_lt_k k_lt_c) | (LowerBoundsDeeper a'lta l t' e) = void $ lemma_too_high t' b_lt_k e
    treeInsert k v (Branch2 l r) | (B2BetweenLR b_lt_k k_lt_c) | (HigherBounds fltf' l t' e) = SameBounds k _ (Branch2 t' r) (InB2L e)
    treeInsert k v (Branch2 l r) | (B2BetweenLR b_lt_k k_lt_c) | (HigherBoundsDeeper fltf' l (Branch2 x y) (InB2L e)) =
      SameBounds k _ (Branch3 x y r) (InB3L e)
    treeInsert k v (Branch2 l r) | (B2BetweenLR b_lt_k k_lt_c) | (HigherBoundsDeeper fltf' l (Branch2 x y) (InB2R e)) =
      SameBounds k _ (Branch3 x y r) (InB3M e)
    treeInsert k v (Branch2 l r) | (B2BetweenLR b_lt_k k_lt_c) | (HigherBoundsDeeper fltf' l (Branch3 x y z) (InB3L e)) =
      SameBoundsDeeper k _ (Branch2 (Branch2 x y) (Branch2 z r)) (InB2L (InB2L e))
    treeInsert k v (Branch2 l r) | (B2BetweenLR b_lt_k k_lt_c) | (HigherBoundsDeeper fltf' l (Branch3 x y z) (InB3M e)) =
      SameBoundsDeeper k _ (Branch2 (Branch2 x y) (Branch2 z r)) (InB2L (InB2R e))
    treeInsert k v (Branch2 l r) | (B2BetweenLR b_lt_k k_lt_c) | (HigherBoundsDeeper fltf' l (Branch3 x y z) (InB3R e)) =
      SameBoundsDeeper k _ (Branch2 (Branch2 x y) (Branch2 z r)) (InB2R (InB2L e))

  treeInsert k v (Branch3 l m r) | (B3Below k_lt_a) with (treeInsert k v l)
    treeInsert k v (Branch3 l m r) | (B3Below k_lt_a) | (SameBounds k l t' y) =
      void (lemma_too_low t' k_lt_a y)
    treeInsert k v (Branch3 l m r) | (B3Below k_lt_a) | (SameBoundsDeeper k l t' y) =
      void (lemma_too_low t' k_lt_a y)
    treeInsert k v (Branch3 l m r) | (B3Below k_lt_a) | (LowerBounds a'lta l t' e) =
      LowerBounds a'lta _ (Branch3 t' m r) (InB3L e)
    treeInsert k v (Branch3 l m r) | (B3Below k_lt_a) | (LowerBoundsDeeper a'lta l (Branch2 x y) (InB2L e)) =
      LowerBoundsDeeper a'lta _ (Branch2 (Branch2 x y) (Branch2 m r)) (InB2L (InB2L e))
    treeInsert k v (Branch3 l m r) | (B3Below k_lt_a) | (LowerBoundsDeeper a'lta l (Branch2 {r0} x y) (InB2R e)) =
      void (lemma_too_low y (trans_tree_lt x r0) e)
    treeInsert k v (Branch3 l m r) | (B3Below k_lt_a) | (LowerBoundsDeeper a'lta l (Branch3 x y z) (InB3L e)) =
      LowerBoundsDeeper a'lta _ (Branch2 (Branch3 x y z) (Branch2 m r)) (InB2L (InB3L e))
    treeInsert k v (Branch3 l m r) | (B3Below k_lt_a) | (LowerBoundsDeeper a'lta l (Branch3 {r1} x y z) (InB3M e)) =
      void (lemma_too_low y (trans_tree_lt x r1) e)
    treeInsert k v (Branch3 l m r) | (B3Below k_lt_a) | (LowerBoundsDeeper a'lta l (Branch3 {r1,r2} x y z) (InB3R e)) =
      void (lemma_too_low z (trans {ctx,x=k} (trans_lt_tree (trans_tree_lt x r1) y) r2) e)
    treeInsert k v (Branch3 l m r) | (B3Below k_lt_a) | (HigherBounds fltf' l t' x) =
      void $ asym fltf' (trans_lt_tree k_lt_a l)
    treeInsert k v (Branch3 l m r) | (B3Below k_lt_a) | (HigherBoundsDeeper fltf' l t' x) =
      void $ asym fltf' (trans_lt_tree k_lt_a l)
  
  treeInsert k v (Branch3 l m r) | (B3Above f_lt_k) with (treeInsert k v r)
    treeInsert k v (Branch3 l m r) | (B3Above f_lt_k) | (SameBounds k r t' y) =
      void (lemma_too_high t' f_lt_k y)
    treeInsert k v (Branch3 l m r) | (B3Above f_lt_k) | (SameBoundsDeeper k r t' y) =
      void (lemma_too_high t' f_lt_k y)
    treeInsert k v (Branch3 l m r) | (B3Above f_lt_k) | (LowerBounds a'lta r t' x) =
      void $ lemma_too_high t' f_lt_k x
    treeInsert k v (Branch3 l m r) | (B3Above f_lt_k) | (LowerBoundsDeeper a'lta r t' x) =
      void $ lemma_too_high t' f_lt_k x
    treeInsert k v (Branch3 l m r) | (B3Above f_lt_k) | (HigherBounds fltf' r t' x) =
      HigherBounds f_lt_k _ (Branch3 l m t') (InB3R x)
    treeInsert k v (Branch3 l m r) | (B3Above f_lt_k) | (HigherBoundsDeeper fltf' r (Branch2 {r0} y z) (InB2L e)) =
      void $ lemma_too_high y (trans_lt_tree r0 z) e
    treeInsert k v (Branch3 l m r) | (B3Above f_lt_k) | (HigherBoundsDeeper fltf' r (Branch2 y z) (InB2R e)) =
      HigherBoundsDeeper f_lt_k _ (Branch2 (Branch2 l m) (Branch2 y z)) (InB2R (InB2R e))
    treeInsert k v (Branch3 l m r) | (B3Above f_lt_k) | (HigherBoundsDeeper fltf' r (Branch3 {r1,r2} y z w) (InB3L e)) =
      void $ lemma_too_high y (trans_lt_tree (trans {ctx} (trans_lt_tree r1 z) r2) w) e
    treeInsert k v (Branch3 l m r) | (B3Above f_lt_k) | (HigherBoundsDeeper fltf' r (Branch3 {r2} y z w) (InB3M e)) =
      void $ lemma_too_high z (trans_lt_tree r2 w) e
    treeInsert k v (Branch3 l m r) | (B3Above f_lt_k) | (HigherBoundsDeeper fltf' r (Branch3 y z w) (InB3R e)) =
      HigherBoundsDeeper f_lt_k _ (Branch2 (Branch2 l m) (Branch3 y z w)) (InB2R (InB3R e))
  
  treeInsert k v (Branch3 l m r) | (B3LowL Refl) =
    let
    e = elemLow l
    (t' ** e') = (treeReplace k v l e) in
    SameBounds k _ (Branch3 t' m r) (InB3L e')

  treeInsert k v (Branch3 l m r) | (B3HighL Refl) =
    let
    e = elemHigh l
    (t' ** e') = (treeReplace k v l e) in
    SameBounds k _ (Branch3 t' m r) (InB3L e')
  
  treeInsert k v (Branch3 l m r) | (B3LowM Refl) =
    let
    e = elemLow m
    (t' ** e') = (treeReplace k v m e) in
    SameBounds k _ (Branch3 l t' r) (InB3M e')
  
  treeInsert k v (Branch3 l m r) | (B3HighM Refl) =
    let
    e = elemHigh m
    (t' ** e') = (treeReplace k v m e) in
    SameBounds k _ (Branch3 l t' r) (InB3M e')
  
  treeInsert k v (Branch3 l m r) | (B3LowR Refl) =
    let
    e = elemLow r
    (t' ** e') = (treeReplace k v r e) in
    SameBounds k _ (Branch3 l m t') (InB3R e')
  
  treeInsert k v (Branch3 l m r) | (B3HighR Refl) =
    let
    e = elemHigh r
    (t' ** e') = (treeReplace k v r e) in
    SameBounds k _ (Branch3 l m t') (InB3R e')

  treeInsert k v (Branch3 l m r) | (B3InL a_lt_k k_lt_b) with (treeInsert k v l)
    treeInsert k v (Branch3 l m r) | (B3InL a_lt_k k_lt_b) | (SameBounds k l t' y) =
      SameBounds k _ (Branch3 t' m r) (InB3L y)
    treeInsert k v (Branch3 l m r) | (B3InL a_lt_k k_lt_b) | (SameBoundsDeeper k l (Branch2 x y) (InB2L e)) =
      SameBoundsDeeper k _ (Branch2 (Branch2 x y) (Branch2 m r)) (InB2L (InB2L e))
    treeInsert k v (Branch3 l m r) | (B3InL a_lt_k k_lt_b) | (SameBoundsDeeper k l (Branch2 x y) (InB2R e)) =
      SameBoundsDeeper k _ (Branch2 (Branch2 x y) (Branch2 m r)) (InB2L (InB2R e))
    treeInsert k v (Branch3 l m r) | (B3InL a_lt_k k_lt_b) | (SameBoundsDeeper k l (Branch3 x y z) (InB3L e)) =
      SameBoundsDeeper k _ (Branch2 (Branch3 x y z) (Branch2 m r)) (InB2L (InB3L e))
    treeInsert k v (Branch3 l m r) | (B3InL a_lt_k k_lt_b) | (SameBoundsDeeper k l (Branch3 x y z) (InB3M e)) =
      SameBoundsDeeper k _ (Branch2 (Branch3 x y z) (Branch2 m r)) (InB2L (InB3M e))
    treeInsert k v (Branch3 l m r) | (B3InL a_lt_k k_lt_b) | (SameBoundsDeeper k l (Branch3 x y z) (InB3R e)) =
      SameBoundsDeeper k _ (Branch2 (Branch3 x y z) (Branch2 m r)) (InB2L (InB3R e))
    treeInsert k v (Branch3 l m r) | (B3InL a_lt_k k_lt_b) | (LowerBounds a'lta l t' x) =
      void $ asym a_lt_k a'lta
    treeInsert k v (Branch3 l m r) | (B3InL a_lt_k k_lt_b) | (LowerBoundsDeeper a'lta l t' x) =
      void $ asym a_lt_k a'lta
    treeInsert k v (Branch3 l m r) | (B3InL a_lt_k k_lt_b) | (HigherBounds fltf' l t' x) =
      void $ asym k_lt_b fltf'
    treeInsert k v (Branch3 l m r) | (B3InL a_lt_k k_lt_b) | (HigherBoundsDeeper fltf' l t' x) =
      void $ asym k_lt_b fltf'
  
  treeInsert k v (Branch3 l m r) | (B3InM c_lt_k k_lt_f) with (treeInsert k v m)
    treeInsert k v (Branch3 l m r) | (B3InM c_lt_k k_lt_f) | (SameBounds k m t' y) =
      SameBounds k _ (Branch3 l t' r) (InB3M y)
    treeInsert k v (Branch3 l m r) | (B3InM c_lt_k k_lt_f) | (SameBoundsDeeper k m (Branch2 {b=ib,c=ic,r0} x y) (InB2L e)) =
      SameBoundsDeeper k _ (Branch2 (Branch2 l x) (Branch2 y r)) (InB2L (InB2R e))
    treeInsert k v (Branch3 l m r) | (B3InM c_lt_k k_lt_f) | (SameBoundsDeeper k m (Branch2 x y) (InB2R e)) =
      SameBoundsDeeper k _ (Branch2 (Branch2 l x) (Branch2 y r)) (InB2R (InB2L e))
    treeInsert k v (Branch3 l m r) | (B3InM c_lt_k k_lt_f) | (SameBoundsDeeper k m (Branch3 x y z) (InB3L e)) =
      SameBoundsDeeper k _ (Branch2 (Branch3 l x y) (Branch2 z r)) (InB2L (InB3M e))
    treeInsert k v (Branch3 l m r) | (B3InM c_lt_k k_lt_f) | (SameBoundsDeeper k m (Branch3 x y z) (InB3M e)) =
      SameBoundsDeeper k _ (Branch2 (Branch3 l x y) (Branch2 z r)) (InB2L (InB3R e))
    treeInsert k v (Branch3 l m r) | (B3InM c_lt_k k_lt_f) | (SameBoundsDeeper k m (Branch3 x y z) (InB3R e)) =
      SameBoundsDeeper k _ (Branch2 (Branch3 l x y) (Branch2 z r)) (InB2R (InB2L e))
    treeInsert k v (Branch3 l m r) | (B3InM c_lt_k k_lt_f) | (LowerBounds a'lta m t' e) =
      void $ asym c_lt_k a'lta    
    treeInsert k v (Branch3 l m r) | (B3InM c_lt_k k_lt_f) | (LowerBoundsDeeper a'lta m t' e) =
      void $ asym c_lt_k a'lta
    treeInsert k v (Branch3 l m r) | (B3InM c_lt_k k_lt_f) | (HigherBounds fltf' m t' e) =
      void $ asym k_lt_f fltf'
    treeInsert k v (Branch3 l m r) | (B3InM c_lt_k k_lt_f) | (HigherBoundsDeeper fltf' m t' e) =
      void $ asym k_lt_f fltf'
      
  treeInsert k v (Branch3 l m r) | (B3InR e_lt_k k_lt_f) with (treeInsert k v r)
    treeInsert k v (Branch3 l m r) | (B3InR e_lt_k k_lt_f) | (SameBounds k r t' e) =
      SameBounds k _ (Branch3 l m t') (InB3R e)
    treeInsert k v (Branch3 l m r) | (B3InR e_lt_k k_lt_f) | (SameBoundsDeeper k r (Branch2 x y) (InB2L z)) =
      SameBoundsDeeper k _ (Branch2 (Branch2 l m) (Branch2 x y)) (InB2R (InB2L z))
    treeInsert k v (Branch3 l m r) | (B3InR e_lt_k k_lt_f) | (SameBoundsDeeper k r (Branch2 x y) (InB2R z)) =
      SameBoundsDeeper k _ (Branch2 (Branch2 l m) (Branch2 x y)) (InB2R (InB2R z))
    treeInsert k v (Branch3 l m r) | (B3InR e_lt_k k_lt_f) | (SameBoundsDeeper k r (Branch3 x y z) (InB3L w)) =
      SameBoundsDeeper k _ (Branch2 (Branch2 l m) (Branch3 x y z)) (InB2R (InB3L w))
    treeInsert k v (Branch3 l m r) | (B3InR e_lt_k k_lt_f) | (SameBoundsDeeper k r (Branch3 x y z) (InB3M w)) =
      SameBoundsDeeper k _ (Branch2 (Branch2 l m) (Branch3 x y z)) (InB2R (InB3M w))
    treeInsert k v (Branch3 l m r) | (B3InR e_lt_k k_lt_f) | (SameBoundsDeeper k r (Branch3 x y z) (InB3R w)) =
      SameBoundsDeeper k _ (Branch2 (Branch2 l m) (Branch3 x y z)) (InB2R (InB3R w))
    treeInsert k v (Branch3 l m r) | (B3InR e_lt_k k_lt_f) | (LowerBounds a'lta r t' e) =
      void $ asym e_lt_k a'lta
    treeInsert k v (Branch3 l m r) | (B3InR e_lt_k k_lt_f) | (LowerBoundsDeeper a'lta r t' e) =
      void $ asym e_lt_k a'lta
    treeInsert k v (Branch3 l m r) | (B3InR e_lt_k k_lt_f) | (HigherBounds fltf' r t' e) =
      void $ asym k_lt_f fltf'
    treeInsert k v (Branch3 l m r) | (B3InR e_lt_k k_lt_f) | (HigherBoundsDeeper fltf' r t' e) =
      void $ asym k_lt_f fltf'

  treeInsert k v (Branch3 l m r) | (B3BetweenLM b_lt_k k_lt_c) with (treeInsert k v m)
    treeInsert k v (Branch3 l m r) | (B3BetweenLM b_lt_k k_lt_c) | (SameBounds k m t' y) =
      void $ lemma_too_low t' k_lt_c y 
    treeInsert k v (Branch3 l m r) | (B3BetweenLM b_lt_k k_lt_c) | (SameBoundsDeeper k m t' y) =
      void $ lemma_too_low t' k_lt_c y 
    treeInsert k v (Branch3 l m r) | (B3BetweenLM b_lt_k k_lt_c) | (LowerBounds a'lta m t' x) =
      SameBounds k _ (Branch3 l t' r) (InB3M x)
    treeInsert k v (Branch3 l m r) | (B3BetweenLM b_lt_k k_lt_c) | (LowerBoundsDeeper a'lta m (Branch2 x y) e) =
      SameBoundsDeeper k _ (Branch2 (Branch2 l x) (Branch2 y r)) (InB2L (InB2R (elemLow x))) -- TODO remove elemLow
    treeInsert k v (Branch3 l m r) | (B3BetweenLM b_lt_k k_lt_c) | (LowerBoundsDeeper a'lta m (Branch3 x y z) e) =
      SameBoundsDeeper k _ (Branch2 (Branch2 l x) (Branch3 y z r)) (InB2L (InB2R (elemLow x))) -- TODO remove elemLow
    treeInsert k v (Branch3 l m r) | (B3BetweenLM b_lt_k k_lt_c) | (HigherBounds fltf' m t' x) =
      void $ lemma_too_low t' k_lt_c x
    treeInsert k v (Branch3 l m r) | (B3BetweenLM b_lt_k k_lt_c) | (HigherBoundsDeeper fltf' m t' x) =
      void $ lemma_too_low t' k_lt_c x  

  treeInsert k v (Branch3 l m r) | (B3BetweenMR d_lt_k k_lt_e) with (treeInsert k v m)
    treeInsert k v (Branch3 l m r) | (B3BetweenMR d_lt_k k_lt_e) | (SameBounds k m t' y) =
      void $ lemma_too_high t' d_lt_k y
    treeInsert k v (Branch3 l m r) | (B3BetweenMR d_lt_k k_lt_e) | (SameBoundsDeeper k m t' y) =
      void $ lemma_too_high t' d_lt_k y
    treeInsert k v (Branch3 l m r) | (B3BetweenMR d_lt_k k_lt_e) | (LowerBounds a'lta m t' x) =
      void $ lemma_too_high t' d_lt_k x
    treeInsert k v (Branch3 l m r) | (B3BetweenMR d_lt_k k_lt_e) | (LowerBoundsDeeper a'lta m t' x) =
      void $ lemma_too_high t' d_lt_k x
    treeInsert k v (Branch3 l m r) | (B3BetweenMR d_lt_k k_lt_e) | (HigherBounds fltf' m t' e) =
      SameBounds k _ (Branch3 l t' r) (InB3M e)
    treeInsert k v (Branch3 l m r) | (B3BetweenMR d_lt_k k_lt_e) | (HigherBoundsDeeper fltf' m (Branch2 x y) e) =
      SameBoundsDeeper k _ (Branch2 (Branch2 l x) (Branch2 y r)) (InB2R (InB2L (elemHigh y))) -- TODO remove elemHigh      
    treeInsert k v (Branch3 l m r) | (B3BetweenMR d_lt_k k_lt_e) | (HigherBoundsDeeper fltf' m (Branch3 x y z) e) =
      SameBoundsDeeper k _ (Branch2 (Branch2 l x) (Branch3 y z r)) (InB2R (InB3M (elemHigh z))) -- TODO remove elemHigh
    
      
