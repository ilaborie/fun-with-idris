module Data.Nat.Primality.Samples

import Data.Nat.Primality


prime2 : Prime 2
prime2 = IsPrime prime2Prf
  where
  prime2Prf : (q : Nat) -> q .|. 2 -> Either (1=q) (2=q)
  prime2Prf 0 div02 = absurd $ z_does_not_divide_Sn div02
  prime2Prf 1 div12 = Left Refl
  prime2Prf 2 div22 = Right Refl
  prime2Prf (S $ S $ S k) (DivRatio (S $ S $ S k) 2 0 {prq = prq}) = 
    absurd prq
  prime2Prf (S $ S $ S k) (DivRatio (S $ S $ S k) 2 (S j) {prq = prq}) = 
    absurd $ uncongNat 1 prq

prime3 : Prime 3
prime3 = IsPrime prime3Prf
  where
  prime3Prf : (q : Nat) -> q .|. 3 -> Either (1=q) (3=q)
  prime3Prf 0 div03 = absurd $ z_does_not_divide_Sn div03
  prime3Prf 1 div13 = Left Refl
  prime3Prf 2 (DivRatio 2 3 0 {prq = prq}) = absurd prq
  prime3Prf 2 (DivRatio 2 3 1 {prq = prq}) = absurd prq
  prime3Prf 2 (DivRatio 2 3 (S (S k)) {prq = prq}) =
    absurd $ uncongNat 3 prq
  prime3Prf 3 div33 = Right Refl
  prime3Prf (S $ S $ S $ S k) (DivRatio (S $ S $ S $ S k) 3 0 {prq = prq}) = 
    absurd prq
  prime3Prf (S $ S $ S $ S k) (DivRatio (S $ S $ S $ S k) 3 (S j) {prq = prq}) = 
    absurd $ uncongNat 3 prq

prime5 : Prime 5
prime5 = IsPrime prime5Prf
  where
  prime5Prf : (q : Nat) -> q .|. 5 -> Either (1=q) (5=q)
  prime5Prf 0 div05 = absurd $ z_does_not_divide_Sn div05
  prime5Prf 1 div15 = Left Refl
  prime5Prf 2 (DivRatio 2 5 0 {prq}) = absurd prq
  prime5Prf 2 (DivRatio 2 5 1 {prq}) = absurd prq
  prime5Prf 2 (DivRatio 2 5 2 {prq}) = absurd prq
  prime5Prf 2 (DivRatio 2 5 3 {prq}) = absurd prq
  prime5Prf 2 (DivRatio 2 5 (S (S (S (S k)))) {prq = prq}) =
    absurd $ uncongNat 5 prq
  prime5Prf 3 (DivRatio 3 5 0 {prq = prq}) = absurd prq
  prime5Prf 3 (DivRatio 3 5 1 {prq = prq}) = absurd prq
  prime5Prf 3 (DivRatio 3 5 (S (S k)) {prq = prq}) =
    absurd $ uncongNat 5 prq  
  prime5Prf 4 (DivRatio 4 5 0 {prq = prq}) = absurd prq
  prime5Prf 4 (DivRatio 4 5 1 {prq = prq}) = absurd prq
  prime5Prf 4 (DivRatio 4 5 (S (S k)) {prq = prq}) =
    absurd $ uncongNat 5 prq  
  prime5Prf 5 div55 = Right Refl
  prime5Prf (S (S (S (S (S (S k)))))) (DivRatio (S (S (S (S (S (S k)))))) 5 0 {prq = prq}) = absurd prq
  prime5Prf (S (S (S (S (S (S k)))))) (DivRatio (S (S (S (S (S (S k)))))) 5 (S j) {prq = prq}) =
    absurd $ uncongNat 5 prq

composite4 : Composite 4
composite4 = IsComposite prime2 {qdivsn=DivRatio 2 4 2}

composite6 : Composite 6
composite6 = IsComposite prime3 {qdivsn=DivRatio 3 6 2}
