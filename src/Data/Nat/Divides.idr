module Data.Nat.Divides

import public Data.Nat
import public Decidable.Equality
import public Decidable.Order.Strict
import public Control.Relation
import Syntax.PreorderReasoning
import Syntax.PreorderReasoning.Generic

import public Data.Nat.LessThan

infixr 11 .|.

public export
data (.|.) : Nat -> Nat -> Type where
  DivRatio :  (q,p,r:Nat)
          -> {auto nonnulr : 0 .<. r}
          -> {auto prq: p = r * q}
          -> q .|. p

divides_5_10 : 5 .|. 10
divides_5_10 = DivRatio 5 10 2

export
eqCongMult : {a,b,n:Nat} -> a=b -> (n*a)=(n*b)
eqCongMult Refl = Refl


-- dividesCongMult' : {n,q,p:Nat} -> Divides q p -> Divides (n*q) (n*p) -> Divides (S n*q) (S n*p)
-- dividesCongMult' {n=0} dqp dnpnq = rewrite plusn0eqn {n=q} in rewrite plusn0eqn {n=p} in dqp
-- dividesCongMult' {n=S m} 
--   (DivRatio q p r1 {nonnulr=nr1} {prq=prq1}) 
--   (DivRatio _ _ r2 {nonnulr=nr2} {prq=prq2}) = ?a

export
multAssociative' : (l, c, r : Nat) -> l * (c * r) = c * (l * r)
multAssociative' l c r = Calc $
  |~ l * (c * r)
  ~~ l * (r * c) ...(cong (l *) $ multCommutative c r)
  ~~ (l * r) * c ...(multAssociative l r c)
  ~~ c * (l * r) ...(multCommutative (l * r) c)

export
dividesCongMult : {n,q,p:Nat} -> q .|. p -> (n*q) .|. (n*p)
dividesCongMult {n} (DivRatio q p r {nonnulr} {prq}) =
  let npnqr = eqCongMult {a=p} {b=r*q} {n=n} prq in
  let prf = (rewrite sym $ multAssociative' n r q in npnqr) in
  DivRatio (n*q) (n*p) r {nonnulr} {prq=prf}

export
not0eqSn : Not (0=S n)
not0eqSn Refl impossible

export
eqSxSyeqxy : S x = S y -> x = y
eqSxSyeqxy Refl = Refl

export
dividesAntisymmetric : {p,q:Nat} -> q .|. p -> p .|. q -> p = q
dividesAntisymmetric (DivRatio q p 0 {nonnulr = nonnulr}) (DivRatio p q i) = absurd $ sltIrreflexive nonnulr
dividesAntisymmetric (DivRatio q p (S k)) (DivRatio p q 0 {nonnulr=nonnuli}) = absurd $ sltIrreflexive nonnuli
dividesAntisymmetric (DivRatio 0 0 (S k) {prq = prq}) (DivRatio 0 0 (S j) {prq=qip}) = Refl
dividesAntisymmetric (DivRatio 0 (S i) (S k) {prq = prq}) (DivRatio (S i) 0 (S j) {prq=qip}) = absurd $ not0eqSn qip
dividesAntisymmetric (DivRatio (S i) 0 (S k) {prq = prq}) (DivRatio 0 (S i) (S j) {prq=qip}) = absurd $ not0eqSn prq
dividesAntisymmetric (DivRatio (S a) (S b) (S k) {prq = akb}) (DivRatio (S b) (S a) (S j) {prq=bja}) = 
  let 
    beqaplusz = eqSxSyeqxy akb 
    aeqbplusz = eqSxSyeqxy bja
  in ?z

export
dividesOrdered : a .<. b -> Not (b .|. a)

export
zdivSn_implies_zeqSn : 0 .|. S n -> 0 = S n
zdivSn_implies_zeqSn (DivRatio 0 (S n) r {prq}) = sym $ Calc $
  |~ S n
  ~~ r * 0   ... (prq)
  ~~ 0       ... (multn0eq0)

export
z_divides_z : 0 .|. 0
z_divides_z = DivRatio 0 0 1

export
z_does_not_divide_Sn : Not (0 .|. S n)
z_does_not_divide_Sn = absurd . zdivSn_implies_zeqSn


