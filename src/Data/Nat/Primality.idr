module Data.Nat.Primality

import public Data.Nat.Primality.Lemmas
import public Data.Nat.Divides
import public Data.Nat.LessThan

import Data.Nat
import Syntax.PreorderReasoning

data Prime : Nat -> Type where
  IsPrime :  {n:Nat}
          -> {auto ngt1: 1 .<. n }
          -> (prf: (q:Nat) -> q .|. n -> Either (1=q) (n=q))
          -> Prime n

data Composite : Nat -> Type where
  IsComposite :  {q,n : Nat}
              -> Prime q
              -> {auto qltn: q .<. n}
              -> {auto qdivsn: q .|. n}
              -> Composite n


notBothPrimeAndComposite : {n:Nat} -> Prime n -> Composite n -> Void
notBothPrimeAndComposite (IsPrime {ngt1} prf) (IsComposite {q} primeq {qltn} {qdivsn}) with (prf q qdivsn)
  notBothPrimeAndComposite (IsPrime {ngt1} prf) (IsComposite {q} (IsPrime {n=q} {ngt1=qgt1} _) {qltn} {qdivsn}) | Left isOne = ltNotEq qgt1 isOne
  notBothPrimeAndComposite (IsPrime {ngt1} prf) (IsComposite {q} primeq {qltn} {qdivsn}) | Right neqq = ltNotEq qltn (sym neqq)



{-
-- The meat of this module: All numbers above 1 are either prime or composite.
--
-- This decision procedure proceeds by using the ¬LPO machinery above to find a number between 2 and p - 1 that divides
-- p. If one exists, then the number is composite, and if it doesn't, the number is prime.
primality : ∀ p → p > 1 → Prime p ⊎ Composite p
primality 0 ()
primality 1 (s≤s ())
primality (suc (suc p)) p>1 = <-rec _ helper p
  
  where
  
  module ∣-Poset = Poset poset
  
  helper : ∀ p 
           → (∀ i 
              → i < p 
              → Prime (2 + i) ⊎ Composite (2 + i)
             ) 
           → Prime (2 + p) ⊎ Composite (2 + p)
         
  helper p f with ¬LPO p (λ i i<p → (2 + i) ∣? (2 + p))
  
    There exists a number i, smaller than p, that divides p. This is
    the tricky case.  Most of the trickiness comes from the fact that
    in the composite case, the ¬LPO gives us a number i smaller than p
    that divides p, but says nothing about whether that number is
    prime or not.  Obviously, we'd like to ask whether that number is
    itself prime or not, but unfortunately it's not obviously
    structurally smaller than p.  If we just naively call primality on
    it, Agda will complain about non-obvious termination.  This is
    where the well-founded recursion comes into play.  We know that i
    < p, and we showed above that _<_ is well founded, so we use the
    well-founded recursion voodoo and call ourselves recursively (f is
    effectively the primality test hidden behind the safe well-founded
    recursion stuff).

  helper p f | inj₁ (i , i<p , 2+i∣2+p) with f i i<p 
  
      -- Aha! i is prime, so we're good and can return that proof in our
      composite proof
       
    helper p f | inj₁ (i , i<p , 2+i∣2+p) | inj₁ P[i] = inj₂ (composite P[i] (s≤s (s≤s i<p)) 2+i∣2+p) 
    
      -- i is composite, but that means it has a prime divisor, which
      we can show transitively divides p! sweet!
     
    helper p f | inj₁ (i , i<p , 2+i∣2+p) | inj₂ (composite P[d] d<c pf) = 
      inj₂ (composite P[d] (<-trans d<c (s≤s (s≤s i<p))) (∣-Poset.trans pf 2+i∣2+p))

    -- The boring case: there exist no numbers between 2 and p - 1 that
      divide p, so transmogrify that proof into our primality
      statement.
      
  helper p f | inj₂ y = inj₁ (prime (s≤s (s≤s z≤n)) lemma₁)
    where
    lemma₀ : ∀ {n p} → 2 + n ≤ 2 + p → 1 + n ≤ 1 + p
    lemma₀ (s≤s 1+n≤1+p) = 1+n≤1+p

    lemma₁ : {d : ℕ} → d ∣ 2 + p → d ≡ 1 ⊎ d ≡ 2 + p
    lemma₁ {d} d∣2+p with d ≟ 1 | d ≟ 2 + p
    lemma₁ d∣2+p | yes refl | yes ()
    lemma₁ d∣2+p | yes d≡1 | no  d≢2+p = inj₁ d≡1
    lemma₁ d∣2+p | no  d≢1 | yes d≡2+p = inj₂ d≡2+p
    lemma₁ {zero} d∣2+p | no d≢1 | no d≢2+p rewrite 0∣⇒≡0 d∣2+p = ⊥-elim (d≢2+p refl)
    lemma₁ {suc zero} d∣2+p | no d≢1 | no d≢2+p = ⊥-elim (d≢1 refl)
    lemma₁ {suc (suc n)} d∣2+p | no d≢1 | no d≢2+p = ⊥-elim (y n (unsplit-≤ (d≢2+p ∘ cong suc) (lemma₀ (∣⇒≤ d∣2+p))) d∣2+p)

-}

primeOrComposite : (n:Nat) -> {auto ngt1:1.<.n} -> Either (Prime n) (Composite n)
primeOrComposite 0 impossible
primeOrComposite 1 {ngt1} = absurd $ sltIrreflexive ngt1
primeOrComposite (S (S k)) = ?aprimeOrComposite_rhs_3

