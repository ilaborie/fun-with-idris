module Data.Decidable.SortedDMap

import Control.Relation
import Decidable.Order.Strict

import Data.Tree23.Types
import Data.Tree23.DecElem
import Data.Tree23.Insert
import Data.Tree23.Delete

export
data SortedDMap : (k : Type) -> (v : k -> Type) -> Type where
  Empty : (ctx: TCtx) => SortedDMap ctx.key ctx.val
  M : (ctx: TCtx) => {a,b: ctx.key} -> (0 depth : Nat) -> Tree23 ctx a b depth -> SortedDMap ctx.key ctx.val


export
empty : {ctx: TCtx} -> SortedDMap ctx.key ctx.val
empty = Empty


treeAt : {ctx: TCtx} -> {a,b: ctx.key} -> (t: Tree23 ctx a b depth) -> (k: ctx.key) -> Elem k t  -> ctx.val k
treeAt (Leaf k v) k InLeaf = v
treeAt (Branch2 l r) k (InB2L e) = treeAt l k e
treeAt (Branch2 l r) k (InB2R e) = treeAt r k e
treeAt (Branch3 l m r) k (InB3L e) = treeAt l k e
treeAt (Branch3 l m r) k (InB3M e) = treeAt m k e
treeAt (Branch3 l m r) k (InB3R e) = treeAt r k e


treeLookup : {ctx: TCtx} -> {a,b: ctx.key} -> (t: Tree23 ctx a b depth) -> (k: ctx.key) -> Dec (e: Elem k t ** ctx.val k)
treeLookup t k with (decElem k t)
  treeLookup t k | (Yes prf) = Yes (prf ** treeAt t k prf)
  treeLookup t k | (No contra) = No (\elem => void $ contra (fst elem))


export
lookup : (x : k) -> SortedDMap k v -> Maybe (v x)
lookup x Empty = Nothing
lookup x (M _ t) with (treeLookup t x)
  lookup x (M _ t) | (Yes (elem ** vx)) = Just vx
  lookup x (M _ t) | (No contra) = Nothing


export
insert : (x : k) -> v x -> SortedDMap k v -> SortedDMap k v
insert k v Empty = M Z (Leaf k v)
insert k v (M _ t) with (treeInsert k v t)
  insert k v (M _ t) | (SameBounds k t t' _) = M _ t'
  insert k v (M _ t) | (SameBoundsDeeper k t t' _) = M _ t'
  insert k v (M _ t) | (LowerBounds _ t t' _) = M _ t'
  insert k v (M _ t) | (LowerBoundsDeeper _ t t' _) = M _ t'
  insert k v (M _ t) | (HigherBounds _ t t' _) = M _ t'
  insert k v (M _ t) | (HigherBoundsDeeper _ t t' _) = M _ t'


export
singleton : {ctx: TCtx} -> (x : ctx.key) -> ctx.val x -> SortedDMap ctx.key ctx.val
singleton x y = M Z (Leaf x y)


export
delete : k -> SortedDMap k v -> SortedDMap k v
delete x Empty = Empty
delete x (M depth t) with (decElem x t)
  delete x (M depth t) | (Yes elemx) with (treeDelete x t elemx)
    delete x (M 0 (Leaf x vx)) | (Yes elemx) | (DeletedLeaf x) = Empty
    delete x (M depth t) | (Yes elemx) | (SameBounds x a_lt_k k_lt_f t t' ne) = M _ t'
    delete x (M (S depth) t) | (Yes elemx) | (SameBoundsShallower x a_lt_k k_lt_f t t' ne) = M _ t'
    delete x (M depth t) | (Yes elemx) | (LowBound k_lt_a' t t' ne) = M _ t'
    delete x (M (S depth) t) | (Yes elemx) | (LowBoundShallower k_lt_a' t t' ne) = M _ t'
    delete x (M depth t) | (Yes elemx) | (HighBound f'_lt_k t t' ne) = M _ t'
    delete x (M (S depth) t) | (Yes elemx) | (HighBoundShallower f'_lt_k t t' ne) = M _ t'
  delete x (M depth t) | (No contra) = M _ t


{-

export
singleton : Ord k => (x : k) -> v x -> SortedDMap k v
singleton k v = insert k v empty

export
insertFrom : Foldable f => f (x : k ** v x) -> SortedDMap k v -> SortedDMap k v
insertFrom = flip $ foldl $ flip $ uncurry insert

export
delete : k -> SortedDMap k v -> SortedDMap k v
delete _ Empty = Empty
delete k (M Z t) =
  case treeDelete Z k t of
    Left t' => (M _ t')
    Right () => Empty
delete k (M (S n) t) =
  case treeDelete (S n) k t of
    Left t' => (M _ t')
    Right t' => (M _ t')

||| Updates or deletes a value based on the decision function
|||
||| The decision function takes information about the presence of the value,
||| and the value itself, if it is present.
||| It returns a new value or the fact that there should be no value as the result.
|||
||| The current implementation performs up to two traversals of the original map
export
update : DecEq k => (x : k) -> (Maybe (v x) -> Maybe (v x)) -> SortedDMap k v -> SortedDMap k v
update k f m = case f $ lookupPrecise k m of
  Just v  => insert k v m
  Nothing => delete k m

||| Updates existing value, if it is present, and does nothing otherwise
|||
||| The current implementation performs up to two traversals of the original map
export
updateExisting : DecEq k => (x : k) -> (v x -> v x) -> SortedDMap k v -> SortedDMap k v
updateExisting k f m = case lookupPrecise k m of
  Just v  => insert k (f v) m
  Nothing => m

export
fromList : Ord k => List (x : k ** v x) -> SortedDMap k v
fromList = foldl (flip (uncurry insert)) empty

export
toList : SortedDMap k v -> List (x : k ** v x)
toList Empty = []
toList (M _ t) = treeToList t

||| Gets the keys of the map.
export
keys : SortedDMap k v -> List k
keys = map fst . toList

export
values : SortedDMap k v -> List (x : k ** v x)
values = toList

treeMap : ({x : k} -> a x -> b x) -> Tree n k a o -> Tree n k b o
treeMap f (Leaf k v) = Leaf k (f v)
treeMap f (Branch2 t1 k t2) = Branch2 (treeMap f t1) k (treeMap f t2)
treeMap f (Branch3 t1 k1 t2 k2 t3)
    = Branch3 (treeMap f t1) k1 (treeMap f t2) k2 (treeMap f t3)

treeTraverse : Applicative f => ({x : k} -> a x -> f (b x)) -> Tree n k a o -> f (Tree n k b o)
treeTraverse f (Leaf k v) = Leaf k <$> f v
treeTraverse f (Branch2 t1 k t2) =
  Branch2
    <$> treeTraverse f t1
    <*> pure k
    <*> treeTraverse f t2
treeTraverse f (Branch3 t1 k1 t2 k2 t3) =
  Branch3
    <$> treeTraverse f t1
    <*> pure k1
    <*> treeTraverse f t2
    <*> pure k2
    <*> treeTraverse f t3

export
map : ({x : k} -> v x -> w x) -> SortedDMap k v -> SortedDMap k w
map _ Empty = Empty
map f (M _ t) = M _ $ treeMap f t

export
foldl : (acc -> (x : k ** v x) -> acc) -> acc -> SortedDMap k v -> acc
foldl f i = foldl f i . values

export
foldr : ((x : k ** v x) -> acc -> acc) -> acc -> SortedDMap k v -> acc
foldr f i = foldr f i . values

export
foldlM : Monad m => (acc -> (x : k ** v x) -> m acc) -> acc -> SortedDMap k v -> m acc
foldlM f i = foldl (\ma, b => ma >>= flip f b) (pure i)

export
foldMap : Monoid m => (f : (x : k) -> v x -> m) -> SortedDMap k v -> m
foldMap f = foldr ((<+>) . uncurry f) neutral

export
null : SortedDMap k v -> Bool
null Empty   = True
null (M _ _) = False

export
traverse : Applicative f => ({x : k} -> v x -> f (w x)) -> SortedDMap k v -> f (SortedDMap k w)
traverse _ Empty = pure Empty
traverse f (M _ t) = M _ <$> treeTraverse f t

||| Merge two maps. When encountering duplicate keys, using a function to combine the values.
||| Uses the ordering of the first map given.
export
mergeWith : DecEq k => ({x : k} -> v x -> v x -> v x) -> SortedDMap k v -> SortedDMap k v -> SortedDMap k v
mergeWith f x y = insertFrom inserted x where
  inserted : List (x : k ** v x)
  inserted = do
    (k ** v) <- toList y
    let v' = (maybe id f $ lookupPrecise k x) v
    pure (k ** v')

||| Merge two maps using the Semigroup (and by extension, Monoid) operation.
||| Uses mergeWith internally, so the ordering of the left map is kept.
export
merge : DecEq k => ({x : k} -> Semigroup (v x)) => SortedDMap k v -> SortedDMap k v -> SortedDMap k v
merge = mergeWith (<+>)

||| Left-biased merge, also keeps the ordering specified  by the left map.
export
mergeLeft : DecEq k => SortedDMap k v -> SortedDMap k v -> SortedDMap k v
mergeLeft = mergeWith const

treeLeftMost : Tree n k v o -> (x : k ** v x)
treeLeftMost (Leaf x y) = (x ** y)
treeLeftMost (Branch2 x _ _) = treeLeftMost x
treeLeftMost (Branch3 x _ _ _ _) = treeLeftMost x

treeRightMost : Tree n k v o -> (x : k ** v x)
treeRightMost (Leaf x y) = (x ** y)
treeRightMost (Branch2 _ _ x) = treeRightMost x
treeRightMost (Branch3 _ _ _ _ x) = treeRightMost x

treeLookupBetween : Ord k => k -> Tree n k v o -> (Maybe (x : k ** v x), Maybe (x : k ** v x))
treeLookupBetween k (Leaf k' v) with (k < k')
  treeLookupBetween k (Leaf k' v) | True = (Nothing, Just (k' ** v))
  treeLookupBetween k (Leaf k' v) | False = (Just (k' ** v), Nothing)
treeLookupBetween k (Branch2 t1 k' t2) with (k < k')
  treeLookupBetween k (Branch2 t1 k' t2) | True = -- k < k'
    let (lower, upper) = treeLookupBetween k t1 in
    (lower, upper <|> pure (treeLeftMost t2))
  treeLookupBetween k (Branch2 t1 k' t2) | False = -- k >= k'
    let (lower, upper) = treeLookupBetween k t2 in
    (lower <|> pure (treeRightMost t1), upper)
treeLookupBetween k (Branch3 t1 k1 t2 k2 t3) with (k < k1)
  treeLookupBetween k (Branch3 t1 k1 t2 k2 t3) | True = treeLookupBetween k (Branch2 t1 k1 t2)
  treeLookupBetween k (Branch3 t1 k1 t2 k2 t3) | False with (k < k2)
    treeLookupBetween k (Branch3 t1 k1 t2 k2 t3) | False | False = treeLookupBetween k (Branch2 t2 k2 t3)
    treeLookupBetween k (Branch3 t1 k1 t2 k2 t3) | False | True = --k1 <= k < k2
      let (lower, upper) = treeLookupBetween k (Branch2 t1 k1 t2) in
      (lower, upper <|> pure (treeLeftMost t3))

||| looks up a key in map, returning the left and right closest values, so that
|||  k1 <= k < k2. If at the end of the beginning and/or end of the sorted map, returns
||| nothing appropriately
export
lookupBetween : k -> SortedDMap k v -> (Maybe (x : k ** v x), Maybe (x : k ** v x))
lookupBetween k Empty = (Nothing, Nothing)
lookupBetween k (M _ t) = treeLookupBetween k t


||| Returns the leftmost (least) key and value
export
leftMost : SortedDMap k v -> Maybe (x : k ** v x)
leftMost Empty = Nothing
leftMost (M _ t) = Just $ treeLeftMost t


||| Returns the rightmost (greatest) key and value
export
rightMost : SortedDMap k v -> Maybe (x : k ** v x)
rightMost Empty = Nothing
rightMost (M _ t) = Just $ treeRightMost t

export
(Show k, {x : k} -> Show (v x)) => Show (SortedDMap k v) where
   show m = "fromList " ++ (show $ toList m)

export
(DecEq k, {x : k} -> Eq (v x)) => Eq (SortedDMap k v) where
  (==) = (==) `on` toList

export
strictSubmap : DecEq k => ({x : k} -> Eq (v x)) => (sub : SortedDMap k v) -> (sup : SortedDMap k v) -> Bool
strictSubmap sub sup = all (\(k ** v) => Just v == lookupPrecise k sup) $ toList sub

-- TODO: is this the right variant of merge to use for this? I think it is, but
-- I could also see the advantages of using `mergeLeft`. The current approach is
-- strictly more powerful I believe, because `mergeLeft` can be emulated with
-- the `First` monoid. However, this does require more code to do the same
-- thing.
export
DecEq k => ({x : k} -> Semigroup (v x)) => Semigroup (SortedDMap k v) where
  (<+>) = merge

||| For `neutral <+> y`, y is rebuilt in `Ord k`, so this is not a "strict" Monoid.
||| However, semantically, it should be equal.
export
DecEq k => Ord k => ({x : k} -> Semigroup (v x)) => Monoid (SortedDMap k v) where
  neutral = empty
-}
