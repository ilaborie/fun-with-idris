module Presentation.PrintfType


--  :   se lit "est de type" ou "a pour type"


data Result : u -> Type where
  Constructor1 : p1 -> p2 -> Result p2
  Constructor2 : p1       -> Result p1

syntaxExample : String -> (p2 : Int) -> Result p2 
syntaxExample p1 p2 = ?hole




























--       Implicit
typeof : {t:Type} -> (x:t) -> Type
typeof x = t




data Equals : (x:t) -> (y:t) -> Type where
  Refl : {t:Type} -> {x:t} -> Equals x x

-- '=' is an infix alias for Equals

typeofSomeStringIsString : typeof "some string" = String
typeofSomeStringIsString = Refl






















printfType : List Char -> Type
printfType []                    = String
printfType ('%' :: 's' :: rest) = String -> printfType rest
printfType ('%' :: 'd' :: rest) = Int    -> printfType rest
printfType ( _  :: rest)        = printfType rest


Message : String
Message = " Some string: '%s'. Then some int: '%d'. End of message. "


printfTypeTest : printfType (unpack Message) = (String -> Int -> String)
printfTypeTest = Refl


