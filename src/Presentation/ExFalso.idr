module Presentation.ExFalso

%hide Void


-- "Ex falso quodlibet" is latin from 
-- "from the false, you get whatever you wish for"

-- Void represents the canonical falsity:
-- a type with no constructible value.
data Void : Type where
  -- no data constructor
  -- => impossible to have an instance

-- Principle of explosion: if we can get an impossible value,
-- we can prove anything.
exFalso : Void -> t
exFalso v impossible -- but we can't.
