module Rover.Optims

import Rover.Move

%default total



public export
optimInsts : Instructions -> Instructions
-- base empty case
optimInsts [] = []
-- optimized cases
optimInsts (R :: L :: xs) = optimInsts xs
optimInsts (L :: R :: xs) = optimInsts xs
optimInsts is@(R :: R :: R :: xs) = optimInsts $ assert_smaller is (L :: xs)
optimInsts is@(L :: L :: L :: xs) = optimInsts $ assert_smaller is (R :: xs)
-- base recursive case
optimInsts (x :: xs) = x :: optimInsts xs
