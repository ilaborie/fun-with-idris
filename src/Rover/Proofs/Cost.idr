module Rover.Proofs.Cost

import Data.Nat
import Rover.Move
import Rover.Cost
import Rover.Optims

import Syntax.PreorderReasoning.Generic

infix 6 <=

-- Introducing a standard shortcut notation for LTE ("Less-Than-or-Equal")

(<=) : Nat -> Nat -> Type
(<=) = LTE

-- Lemmas about LTE relation

lteReflexive : {n:Nat} -> n <= n
lteReflexive {n = 0} = LTEZero
lteReflexive {n = (S k)} = LTESucc lteReflexive

lteSuccNRight : {m,n:Nat} -> n <= (m + n)
lteSuccNRight {m=0}     {n=0}     = LTEZero
lteSuccNRight {m=0}     {n=(S k)} = LTESucc lteReflexive
lteSuccNRight {m=(S m)} {n=0}     = LTEZero
lteSuccNRight {m=(S m)} {n=(S k)} = LTESucc $ CalcWith $
  |~ k
  <~ S k       ... ( lteSuccRight lteReflexive )
  <~ m + (S k) ... ( lteSuccNRight )














-- Optimizing given a cost function

optimCost :  (is:Instructions)
          -> cost (optimInsts is) <= cost is
-- boring cases
optimCost [] = LTEZero
optimCost (A :: xs) = LTESucc (LTESucc (optimCost xs))
optimCost (R :: []) = LTESucc LTEZero
optimCost (R :: (A :: xs)) = LTESucc (LTESucc (LTESucc (optimCost xs)))
optimCost (R :: (R :: [])) = LTESucc (LTESucc LTEZero)
optimCost (R :: (R :: (A :: xs))) = LTESucc (LTESucc (LTESucc (LTESucc (optimCost xs))))
optimCost (L :: []) = LTESucc LTEZero
optimCost (L :: (A :: xs)) = LTESucc (LTESucc (LTESucc (optimCost xs)))
optimCost (L :: (L :: [])) = LTESucc (LTESucc LTEZero)
optimCost (L :: (L :: (A :: xs))) = LTESucc (LTESucc (LTESucc (LTESucc (optimCost xs))))
-- optimized cases
optimCost (R :: L :: xs) = CalcWith $
  |~ cost (optimInsts (R :: L :: xs))
  ~~ cost (optimInsts xs)            ...( Refl )
  <~ cost xs                         ...( optimCost xs )
  <~ (S $ S $ cost xs)               ...( lteSuccNRight {m=2} {n=cost xs} )

optimCost (L :: (R :: xs)) = CalcWith $
  |~ cost (optimInsts (L :: R :: xs))
  ~~ cost (optimInsts xs)            ...( Refl )
  <~ cost xs                         ...( optimCost xs )
  <~ S (S $ cost xs)                 ...( lteSuccNRight {m=2} {n=cost xs} )

optimCost (R :: R :: L :: xs) = CalcWith $
  |~ cost (optimInsts (R :: R :: L :: xs))
  ~~ cost (R :: optimInsts xs)       ...( Refl )
  ~~ S (cost (optimInsts xs))        ...( Refl )
  <~ S (cost xs)                     ...( LTESucc $ optimCost xs )
  <~ S ( S $ S (cost xs) )           ...( lteSuccNRight {m=2} {n=S $ cost xs} )

optimCost (L :: L :: R :: xs) =  CalcWith $
  |~ cost (optimInsts (L :: L :: R :: xs))
  ~~ cost (L :: optimInsts xs)       ...( Refl )
  ~~ S (cost (optimInsts xs))        ...( Refl )
  <~ S (cost xs)                     ...( LTESucc $ optimCost xs )
  <~ S ( S $ S (cost xs) )           ...( lteSuccNRight {m=2} {n=S $ cost xs} )

optimCost (R :: R :: R :: xs) = CalcWith $
  |~ cost (optimInsts (R :: R :: R :: xs))
  ~~ cost (optimInsts (L :: xs))     ...( Refl )
  <~ cost (L :: xs)                  ...( optimCost _)
  ~~ S (cost xs)                     ...( Refl )
  <~ S ( S $ S (cost xs) )           ...( lteSuccNRight {m=2} {n=S $ cost xs} )

optimCost (L :: L :: L :: xs) = CalcWith $
  |~ cost (optimInsts (L :: L :: L :: xs))
  ~~ cost (optimInsts (R :: xs))     ...( Refl )
  <~ cost (R :: xs)                  ...( optimCost _)
  ~~ S (cost xs)                     ...( Refl )
  <~ S ( S $ S (cost xs) )           ...( lteSuccNRight {m=2} {n=S $ cost xs} )




optimCost' :  (is:Instructions)
           -> cost' (optimInsts is) <= cost' is
optimCost' [] = LTEZero
optimCost' (A :: xs) = LTESucc (LTESucc (optimCost' xs))
optimCost' (R :: []) = LTESucc (LTESucc (LTESucc LTEZero))
optimCost' (L :: []) = LTESucc LTEZero
optimCost' (R :: (A :: xs)) = LTESucc $ LTESucc $ LTESucc $ LTESucc $ LTESucc $ optimCost' xs
optimCost' (R :: (R :: [])) = LTESucc $ LTESucc $ LTESucc $ LTESucc $ LTESucc $ LTESucc LTEZero
optimCost' (R :: (R :: (A :: xs))) = LTESucc $ LTESucc $ LTESucc $ LTESucc $ LTESucc $ LTESucc $ LTESucc  $ LTESucc $ optimCost' xs
optimCost' (L :: (A :: xs)) = LTESucc (LTESucc (LTESucc (optimCost' xs)))
optimCost' (L :: (L :: [])) = LTESucc (LTESucc LTEZero)
optimCost' (L :: (L :: (A :: xs))) = LTESucc (LTESucc (LTESucc (LTESucc (optimCost' xs))))

optimCost' (R :: (L :: xs)) = CalcWith $
  |~ cost' (optimInsts xs)
  <~ cost' xs ...( optimCost' xs )
  <~ 4 + (cost' xs) ...( lteSuccNRight {m=4} {n=cost' xs} )
optimCost' (L :: (R :: xs)) = CalcWith $
  |~ cost' (optimInsts xs)
  <~ cost' xs ...( optimCost' xs )
  <~ 4 + (cost' xs) ...( lteSuccNRight {m=4} {n=cost' xs} )

optimCost' (R :: (R :: (L :: xs))) = LTESucc $ LTESucc $ LTESucc $ CalcWith $
  |~ cost' (optimInsts xs)
  <~ cost' xs ... ( optimCost' xs )
  <~ 4 + (cost' xs) ...( lteSuccNRight {m=4} {n=cost' xs} )
optimCost' (L :: (L :: (R :: xs))) = LTESucc $ CalcWith $
  |~ cost' (optimInsts xs)
  <~ cost' xs ... ( optimCost' xs )
  <~ 4 + (cost' xs) ...( lteSuccNRight {m=4} {n=cost' xs} )
  
optimCost' (R :: (R :: (R :: xs))) = CalcWith $
  |~ cost' (optimInsts (L :: xs))
  <~ cost' (L :: xs) ... ( optimCost' _ )
  ~~ 1 + cost' xs ... ( Refl )
  <~ 9 + cost' xs ... ( lteSuccNRight {m=8} {n=1+(cost' xs)} )
optimCost' (L :: (L :: (L :: xs))) = CalcWith $
  |~ cost' (optimInsts (R :: xs))
  <~ cost' (R :: xs) ... ( optimCost' _ )
  ~~ 3 + cost' xs ... ( Refl )
