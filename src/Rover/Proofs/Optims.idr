module Rover.Proofs.Optims

import Syntax.PreorderReasoning

import Rover.Move
import Rover.Optims
import public Rover.Proofs.Rotation

%default total

mutual

  public export
  optimInstsStable :  (p:RoverPos)
                   -> (is:Instructions)
                   -> move (optimInsts is) p
                    = move             is  p
  -- boring cases
  optimInstsStable p [] = Refl
  optimInstsStable p (A :: xs) = optimInstsStable (advance p) xs
  optimInstsStable p [R] = Refl
  optimInstsStable p [R, R] = Refl
  optimInstsStable p (R :: A :: xs) = optimInstsStable (advance (rotate Right p)) xs
  optimInstsStable p (R :: R :: A :: xs) = optimInstsStable (advance (rotate Right (rotate Right p))) xs
  optimInstsStable p [L] = Refl
  optimInstsStable p [L, L] = Refl
  optimInstsStable p (L :: A :: xs) = optimInstsStable (advance (rotate Left p)) xs
  optimInstsStable p (L :: L :: A :: xs) = optimInstsStable (advance (rotate Left (rotate Left p))) xs
  -- simple optimized cases
  optimInstsStable p (R :: L :: xs) = sym $ Calc $
    |~ move xs (rotate Left (rotate Right p))
    ~~ move xs p                             ... (cong (move xs) $ rotateLRCancels p)
    ~~ move (optimInsts xs) p                ... (sym $ optimInstsStable p xs)
  optimInstsStable p (L :: R :: xs) = sym $ Calc $
    |~ move xs (rotate Right (rotate Left p))
    ~~ move xs p                             ... (cong (move xs) $ rotateRLCancels p)
    ~~ move (optimInsts xs) p                ... (sym $ optimInstsStable p xs)
  optimInstsStable p (R :: R :: L :: xs) = sym $ Calc $
    |~ move xs (rotate Left (rotate Right (rotate Right p)))
    ~~ move xs (rotate Right p)              ... (cong (move xs) $ rotateLRCancels _)
    ~~ move (optimInsts xs) (rotate Right p) ... (sym $ optimInstsStable _ xs)
  optimInstsStable p (L :: L :: R :: xs) = sym $ Calc $
    |~ move xs (rotate Right (rotate Left (rotate Left p)))
    ~~ move xs (rotate Left p)               ... (cong (move xs) $ rotateRLCancels _)
    ~~ move (optimInsts xs) (rotate Left p)  ... (sym $ optimInstsStable _ xs)
  -- recursive optimized cases
  optimInstsStable p is@(R :: R :: R :: xs) = sym $ moveOptimPrefixRRR xs p
  optimInstsStable p is@(L :: L :: L :: xs) = sym $ moveOptimPrefixLLL xs p


  -- ========= --
  --   LEMMAS  --
  -- ========= --

  moveOptimPrefixRRR :  (is : List Instruction)
                     -> (p : RoverPos)
                     -> move is (rotate Right (rotate Right (rotate Right p)))
                      = move (optimInsts (L :: is)) p
  moveOptimPrefixRRR is p = Calc $
     |~ move is (rotate Right (rotate Right (rotate Right p)))
     ~~ move is (rotate Left p) ... (cong (move is) $ rotateRRRisL p)
     ~~ move (optimInsts (L :: is)) p ... (moveOptimPrefixL is p)


  moveOptimPrefixRR :  (is : List Instruction)
                    -> (p : RoverPos)
                    -> move is (rotate Right (rotate Right p))
                     = move (optimInsts (R :: (R :: is))) p
  moveOptimPrefixRR [] p = Refl
  moveOptimPrefixRR (A :: is) p = sym $ optimInstsStable _ is
  moveOptimPrefixRR (L :: is) p = Calc $
    |~ move is (rotate Left (rotate Right (rotate Right p)))
    ~~ move is (rotate Right p)                ... (cong (move is) $ rotateLRCancels _)
    ~~ move (optimInsts is) (rotate Right p)   ... (sym $ optimInstsStable _ _)
  moveOptimPrefixRR (R :: is) p = moveOptimPrefixRRR is p



  moveOptimPrefixR :  (is : List Instruction)
                   -> (p : RoverPos)
                   -> move is (rotate Right p)
                    = move (optimInsts (R :: is)) p
  moveOptimPrefixR [] p = Refl
  moveOptimPrefixR (A :: is) p = sym $ optimInstsStable _ is
  moveOptimPrefixR (L :: is) p = Calc $
    |~ move is (rotate Left (rotate Right p))
    ~~ move is p                               ... (cong (move is) $ rotateLRCancels p)
    ~~ move (optimInsts is) p                  ... (sym $ optimInstsStable p is)
  moveOptimPrefixR (R :: is) p = moveOptimPrefixRR is p



  moveOptimPrefixLLL :  (is : List Instruction)
                     -> (p : RoverPos)
                     -> move is (rotate Left (rotate Left (rotate Left p)))
                      = move (optimInsts (R :: is)) p
  moveOptimPrefixLLL is p = Calc $
     |~ move is (rotate Left (rotate Left (rotate Left p)))
     ~~ move is (rotate Right p)               ... (cong (move is) $ rotateLLLisR p)
     ~~ move (optimInsts (R :: is)) p          ... (moveOptimPrefixR is p)



  moveOptimPrefixLL :  (is : List Instruction)
                    -> (p : RoverPos)
                    -> move                         is    (rotate Left (rotate Left p))
                     = move (optimInsts (L :: (L :: is)))                           p
  moveOptimPrefixLL [] p = Refl
  moveOptimPrefixLL (A :: is) p = sym $ optimInstsStable _ is
  moveOptimPrefixLL (R :: is) p = Calc $
    |~ move is (rotate Right (rotate Left (rotate Left p)))
    ~~ move is (rotate Left p)                 ... (cong (move is) $ rotateRLCancels _)
    ~~ move (optimInsts is) (rotate Left p)    ... (sym $ optimInstsStable _ _)
  moveOptimPrefixLL (L :: is) p = moveOptimPrefixLLL is p



  moveOptimPrefixL :  (is : Instructions)
                   -> (p : RoverPos)
                   -> move                   is  (rotate Left p)
                    = move (optimInsts (L :: is))             p
  moveOptimPrefixL [] p = Refl
  moveOptimPrefixL (A :: is) p = sym $ optimInstsStable _ is
  moveOptimPrefixL (R :: is) p = Calc $
    |~ move is (rotate Right (rotate Left p))
    ~~ move is p                              ... (cong (move is) $ rotateRLCancels p)
    ~~ move (optimInsts is) p                 ... (sym $ optimInstsStable p is)
  moveOptimPrefixL (L :: is) p = moveOptimPrefixLL is p
