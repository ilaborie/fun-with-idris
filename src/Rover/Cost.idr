module Rover.Cost

import Rover.Move

public export
CostFn : Type
CostFn = Instructions -> Nat

public export
cost : CostFn
cost [] = 0
cost (A :: is) = 2 + cost is
cost (_ :: is) = 1 + cost is



-- What if the rover could only turn left?  For intance because its
-- engineers determined that it was easier to built, lighter, etc. if
-- it was done this way; or because some gear has broken.  Turning
-- right would have to be implemented by turning left three times.  Is
-- our optimization still efficient?
public export
cost' : CostFn
cost' [] = 0
cost' (A :: is) = 2 + cost' is
cost' (L :: is) = 1 + cost' is
cost' (R :: is) = 3 + cost' is
