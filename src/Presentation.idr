module Presentation

import public Presentation.BTree
import public Presentation.CurryHoward
import public Presentation.Decidability
import public Presentation.Equality
import public Presentation.ExcludedMiddle
import public Presentation.ExFalso
import public Presentation.Head
import public Presentation.Id
import public Presentation.PrintfType