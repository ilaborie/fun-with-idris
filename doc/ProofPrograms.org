#+TITLE:     Le typage dépendant pour structurer sa pensée


:AUTHOR:    Guillaume Andrieu

:COMPANY:   MonkeyPatch

:EMAIL:     guillaume@monkeypatch.io
:MASTODON:  glmxndr@mathstodon.xyz




* Pas ce genre de dépendance (quoique)


*Moi:*     Je peux pas, je prépare ma conf sur les types dépendants.


*Ma meuf:* Ah cool. Des types comme toi, addicts à leur ordi ?



* Systèmes de typage générique

  #+begin_src java
public <T> T head(List<T> ts) { 
  return ts.get(0);
}
  #+end_src

Dans ces systèmes, les types sont régis par un langage
différent de celui des valeurs qui les habitent.

* Système de typage dépendant

Dans un système à typage dit dépendant, les types sont eux-mêmes des
valeurs "de premier ordre".

On les construit, les calcule, les manipule de la même manière que les
valeurs qui les habitent.

#+begin_src idris2

import Presentation.PrintfType

#+end_src

* Une révolution en cours

Les mathématiques s'automatisent.

"StdLib for maths":

+ UniMath : https://github.com/UniMath
+ TypeTopology : https://github.com/martinescardo/TypeTopology

Les outils dits "assistants de preuves" (Coq, Agda, Lean) permettent
de formaliser les théorèmes et leurs preuves.

Le monde de la programmation est au cœur de cette révolution.

* La (vieille) quête des fondements

Depuis le XIXe siècle, les mathématiciens tentent de formaliser un
socle commun qui permette de fonder tout le reste des mathématiques.

- XIXe: Logique des prédicats de second ordre
- 1900: Théorie des ensembles
- 1930: Théories de la calculabilité
        (/Machines de Turing/ / /Lambda-calcul/)
- 1970: Théorie des types dépendants
- 2010: Théorie des types homotopiques

* Classiques vs constructivistes

Au début du XXe siècle, deux grandes familles s'opposent chez les
mathématiciens.

- Les "classiques" qui acceptent le principe de tiers-exclus
  (/excluded middle/)
  + Globalement les logicistes
                    (les maths sont _réelles_)     (Russell /et al./) 
             et les formalistes 
                    (les maths sont des "jeux")  (Hilbert /et al./)

- Les constructivistes qui le rejettent
  + Globalement les intuitionnistes              (Brouwer /et al./) 
                    (les maths sont des _constructions_ mentales)

* /Tertium non datur/

Décidabilité:      pour _une_ proposition ~P~ donnée
                      
              j'ai une preuve soit de ~P~, soit de ~Non P~


Tiers-Exclus:     _toute_ proposition est décidable


Notamment, avec le Tiers-Exclus: ~Non (Non P) => P~

* On n'élimine pas les double négations

Le langage naturel désapprouve le Tiers-Exclus:
  la double négation a du sens.

Exemple:
  "ce n'est pas un échec" n'implique pas "c'est un succès"

* La vraie source de non-calculabilité

#+begin_quote

Brouwer réalisa que la vraie source de non-calculabilité des
fonctions des mathématiques classiques est la loi du tiers-exclus.

  — /Constructive Mathematics and Computer Programming/ 
      Per Martin-Löf, 1984

#+end_quote

* L'arnaque du Tiers-Exclus — Drame en 2 actes

*Acte 1* - /autour d'une lanterne/
  Le Génie: Soit ~A)~ tu me donnes 1M€ et j'exhauce
                    n'importe quel voeu, 
            soit ~B)~ je te donne 1M€.
            Mais /je/ choisis A) ou B).
            Je choisis... l'option ~A)~.
  Le Naïf:  Ok!
*Acte 2* - /vingt ans après/
  Le Naïf:  (/en sueur/) Voici 1M€.
  Le Génie: Bravo. Mais en fait, je choisis ~B)~. Voici 1M€.

Cf. /Call-by-Value is Dual to Call-by-Name/, Wadler 2003

* Mais programmer c'est construire

La logique sous-jacente aux programmes "utiles" est 
une logique constructiviste, pas classique.

Dans cette logique, dire: "_il existe_ un ~x~ tel que (/kekchose/ ~x~)", 

c'est effectivement dire: " _ci-joint_ le ~x~ _avec_ le (/kekchose/ ~x~)".

* Correspondance entre logique et types

La correspondance (dite de Curry Howard) entre logiques
constructivistes et types permet de passer d'un langage à l'autre.

| Logique                 | Type (en Idris2) |
|-------------------------+------------------|
| Vrai                    | ~Unit~ (singleton) |
| Faux                    | ~Void~ (type vide) |
|-------------------------+------------------|
| a implique b            | ~a -> b~           |
| non a = a implique Faux | ~a -> Void~        |
| a et b                  | ~(a, b)~           |
| a ou b                  | ~Either a b~       |
|-------------------------+------------------|
| Pour tout x, P x        | ~(x:t) -> P x~     |
| Il existe x tel que P x | ~(x:t  ** P x)~    |


  — /The formulae-as-types notion of construction/
      William A. Howard, rédigé en 1969

* Tiers-Exclus selon Curry-Howard

#+begin_src idris2

data Void : Type where
  -- Pas de constructeur de données

Not : Type -> Type
Not   t    =  (t -> Void)

excludedMiddle :  (t : Type)        -- Pour tout Type t
               -> Either t (Not t)  -- soit t, soit Non t
excludedMiddle t = ?noProofInConstructivistLogic

#+end_src

* Ceci est une preuve

#+begin_src idris2

greeting : String        -- Proposition "String est vrai"
greeting = "Hello world" -- Preuve (par l'exemple)

#+end_src

Quand on regarde le code avec des lunettes de logicien,
tout programme est une preuve, dans le système formel
que constitue le système de typage.

* Mais de quoi est-ce une preuve?

Quatre implémentations de la même "proposition" ~Nat -> Nat~.

#+begin_src idris2
data Nat : Type where
  Z : Nat        -- Zero
  S : Nat -> Nat -- Successor

two : Nat
two = S (S Z)

idNat     : Nat   -> Nat
idNat       n     =  n

plusTwo   : Nat   -> Nat
plusTwo     n     =  (S (S n))

minusOneS : Nat   -> Nat
minusOneS   Z     =  Z
minusOneS   (S n) =  n

#+end_src

* On ne peut pas tout prouver (1)

Il est capital, dans un systême de typage, de ne pas pouvoir tout
prouver. C'est la base de la notion de fausseté en logique.

#+begin_src idris2

-- Principe d'explosion
exfalso : Void -> t
exfalso   v     = ?what

#+end_src

"Faux" correspond à ~Void~, un type sans valeur.

"Faux" n'a pas de preuve, car sinon tout a une preuve. 
(Principe d'explosion.)

* On ne peut pas tout prouver (2)




#+begin_src idris2

-- Principe d'explosion
exfalso : Void -> t
exfalso   v impossible

#+end_src

Idris sait qu'il n'y a pas de constructeur de donnée pour ~Void~, donc
il sait que ~v~ est impossible.

~exfalso~ est une fonction valide. On ne peut jamais l'appeler, mais on
peut faire comme si.

* Typescript: "Days since never: 0"

#+begin_src js

const nevers : never[] = [];

const oops : never = nevers[0]; // ¯\_(ツ)_/¯ 

#+end_src

     « But typescript is unsound by design! »
 
* Java: "Days since never: NullPointerException"

#+begin_src java
class Never {
    private Never() {}
}
  #+end_src

 #+begin_src java 
public static void main() { 
    Never oops = null; // ¯\_(ツ)_/¯
}
#+end_src

* Implementations for free!

- Le type peut spécifier, documenter, voire définir un programme.

- Dans un système de typage assez puissant, un type suffisamment précis
  force une seule implémentation.

- Une proposition suffisamment précise n'a qu'une seule preuve.
  (Il suffit d'encoder la preuve dans la proposition...)

* Tautologie

#+begin_src idris2
data Nat : Type where  -- Natural numbers
  Z : Nat              -- zero
  S : (m:Nat) -> Nat   -- successor of m

idNat     : Nat -> Nat
idNat       n    = n

namespace Evil
  idNat : Nat -> Nat
  idNat   n    = Z

id : t -> t
id   x =  x

#+end_src

* Les listes c'est comme les poissons

(Ça pourrit par la tête.)

#+begin_src idris2
import Presentation.Head
#+end_src

* Type - Define - Refine

On a vu qu'on peut trop raffiner ses types.

Mais comment savoir où s'arrêter?

Quelles sont les propriétés utiles (en jargon les "invariants") qu'il
est bon d'encoder dans les types?

* Optimisation (non prématurée)

Dans cet exemple, on code une fonction qui optimise un calcul en
modifiant les données d'entrée avant d'appliquer le calcul.

On prouve que la modification n'a pas d'effet sur le calcul.

Et on prouve qu'on a effectivement optimisé quelque chose.

#+begin_src idris2
import Rover
#+end_src

* Ceci n'est pas un programme politique

Trois principes de programmation utiles:

+ pureté - /principe de rationalité de base/
  - les mêmes causes entraînent les mêmes conséquences
  - le comportement de mon code est prédictible

+ totalité - /base du constructivisme/
  - toute donnée d'entrée fournit une donnée de sortie
  - pas Turing-complet 

+ décidabilité - /tiers exclus restreint/
  - si je n'ai pas ce que je veux, au moins
    j'ai une explication de pourquoi je ne peux pas l'avoir

* Un outil de conception

On peut utiliser les types dépendants comme "tableau blanc" pour la
conception.

Cela permet d'identifier les invariants importants d'un système
(donc savoir sur quels types de tests appuyer).

* Exemple d'aide à la conception

#+begin_src idris2
import Data.Tree23
#+end_src

* Merci

Points clés:

- Programmer, c'est construire.

- Les types sont des propositions logiques.
  Les programmes sont leurs preuves.

- Un bon type vaut dix tests.

- Les types dépendants permettent de "tout" formaliser.

- Regarder son code avec des lunettes de logicien.

